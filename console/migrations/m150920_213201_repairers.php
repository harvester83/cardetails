<?php

use yii\db\Migration;

class m150920_213201_repairers extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%repairers}}', [
            'repairer_id' => $this->primaryKey(),
            'profession' => $this->string()->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'address' => $this->string(255)->defaultValue(''),
            'phone' => $this->string()->defaultValue(0),
            'date' => $this->date()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%repairers}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
