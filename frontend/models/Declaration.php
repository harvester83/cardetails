<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

class Declaration extends Model
{
    /**
     *  int
     */
    const MODERATED = 1;

    /**
     * @var int
     */
    private $limit = 5;

    /**
     * @return array|\yii\db\DataReader
     */
    public function getDeclarations($pageNum)
    {
        $start = !empty($pageNum) ? ($pageNum - 1) * $this->limit : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand('
           SELECT `declaration_id`, `moderation`, `product_name`, `editor`, `buyer`, `phone`, `date`
           FROM `declarations`
           WHERE `moderation` = :moderation
           ORDER BY `date` DESC
           LIMIT :start, :limit
        ');

        $command->bindParam(':start', $start);
        $command->bindParam(':limit', $this->limit);
        $command->bindValue(':moderation', self::MODERATED);

        $declarationData =  $command->query();
        $declarationData = $declarationData->readAll();

        return $declarationData;
    }

    /**
     * @param int $productName
     * @param string $editor
     * @param string $buyer
     * @param string $email
     * @param $phone
     * @param int $moderation
     * @param int $date
     * @return bool
     * @throws \yii\db\Exception
     */
    public function sendDeclaration($productName, $editor, $buyer, $email, $phone, $moderation, $date)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            INSERT `declarations`
            SET `product_name` = :productName,
                `editor` = :editor,
                `buyer` = :buyer,
                `email` = :email,
                `phone` = :phone,
                `moderation` = :moderation,
                `date` = :date
            ');

        $command->bindParam(':productName', $productName);
        $command->bindParam(':editor', $editor);
        $command->bindParam(':buyer', $buyer);
        $command->bindParam(':email', $email);
        $command->bindParam(':phone', $phone);
        $command->bindParam(':moderation', $moderation);
        $command->bindParam(':date', $date);

        $command->execute();

        return true;
    }

    /**
     * @return int float
     */
    public function getPagesCount()
    {
        $connection = Yii::$app->db;
        $totalCountRows = $connection->createCommand('
            SELECT COUNT(*)
            FROM `declarations`
        ');

        $totalCountRows = $totalCountRows->queryScalar();
        $numPagination = ceil($totalCountRows / $this->limit);

        return $numPagination;
    }
}