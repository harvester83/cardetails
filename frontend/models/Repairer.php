<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\db\mssql;

class Repairer extends Model
{
    const TABLE_NAME = 'posts';
    
    private $limit = 5;

    /**
     * @param $pageNum
     * @return array|\yii\db\DataReader
     */
    public function getRepairers($pageNum)
    {
        $start = !empty($pageNum) ? ($pageNum - 1) * $this->limit : 1;
        $connection = Yii::$app->db;

        $command = $connection->createCommand("
                        SELECT *
                        FROM `repairers`
                        ORDER BY `date` DESC
                        LIMIT :start, :limit
                    ");

        $command->bindParam(':start', $start);
        $command->bindParam(':limit', $this->limit);

        $dataReader =  $command->query();
        $dataReader = $dataReader->readAll();

        return $dataReader;
    }

    /**
     * @param $postId
     * @param $profile
     * @param $fullName
     * @param $address
     * @param $contactNum
     * @param $date
     * @return bool
     * @throws \yii\db\Exception
     */
    public function editPost($postId, $profile, $fullName, $address, $contactNum, $date)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            UPDATE `repairers`
            SET `profile` = :profile,
                `full_name` = :fullName,
                `address` = :address,
                `contact_num` = :contactNum,
                `date` = :date
            WHERE `id`=:id
        ');

        $command->bindValues(array(
                ':profile' => $profile,
                ':fullName' => $fullName,
                ':address' => $address,
                ':contactNum' => $contactNum,
                ':date' => $date,
                ':id' => $postId,
            )
        );

        $command->execute();

        return true;
    }

    /**
     * @return int float
     */
    public function getPagesCount()
    {
        $connection = Yii::$app->db;
        $totalCountRows = $connection->createCommand('
            SELECT COUNT(*)
            FROM `repairers`
        ');

        $totalCountRows = $totalCountRows->queryScalar();
        $numPagination = ceil($totalCountRows / $this->limit);

        return $numPagination;
    }
}

