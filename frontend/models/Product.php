<?php

namespace frontend\models;

use backend\models\Product as BackendProduct;
use Yii;
use yii\base\Model;
use yii\db\mssql;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Product extends Model
{

    const LAST_PRODUCT_COUNT = 12;
    const MODERATED = 1;
    const PRODUCT_TYPE_GENERAL = 4;

    private $countProductEachPage = 5;

    private $productCategoryType = array(
        1 => '#interior',
        2 => '#exterior',
        3 => '#action-part',
        4 => '#general',
    );

    private $countries = array(
        1 => 'Amerika',
        2 => 'Almaniya',
        3 => 'Fransa',
        4 => 'Danimarka',
        5 => 'Ukrayina',
        6 => 'Rusiya',
        7 => 'Norveç',
        8 => 'İsvecrə',
        9 => 'İrlandiya',
        10 => 'Polşa',
    );

    private $cities = array(
        1 => 'Astara',
        2 => 'Ağcabədi',
        3 => 'Ağdam',
        4 => 'Ağdaş',
        5 => 'Ağdərə',
        6 => 'Ağstafa',
        7 => 'Ağsu',
        8 => 'Bakı',
        9 => 'Balakən',
        10 => 'Beyləqan',
        11 => 'Biləsuvar',
        12 => 'Bərdə',
        13 => 'Culfa',
        14 => 'Cəbrayıl',
        15 => 'Cəlilabad',
        16 => 'Daşkəsən',
        17 => 'Dəliməmmədli',
        18 => 'Füzuli',
        19 => 'Goranboy',
        20 => 'Göygöl',
        21 => 'Göytəpə',
        22 => 'Göyçay',
        23 => 'Gədəbəy',
        24 => 'Gəncə',
        25 => 'Horadiz',
        26 => 'Kürdəmir',
        27 => 'Kəlbəcər',
        28 => 'Laçın',
        29 => 'Lerik',
        30 => 'Liman',
        31 => 'Lənkəran',
        32 => 'Masallı',
        33 => 'Mingəçevir',
        34 => 'Naftalan',
        35 => 'Naxçıvan',
        36 => 'Neftçala',
        37 => 'Ordubad',
        38 => 'Oğuz',
        39 => 'Qax',
        40 => 'Qazax',
        41 => 'Qobustan',
        42 => 'Quba',
        43 => 'Qubadlı',
        44 => 'Qusar',
        45 => 'Qəbələ',
        46 => 'Saatlı',
        47 => 'Sabirabad',
        48 => 'Salyan',
        49 => 'Samux',
        50 => 'Siyəzən',
        51 => 'Sumqayıt',
        52 => 'Tovuz',
        53 => 'Tərtər',
        54 => 'Ucar',
        55 => 'Xankəndi',
        56 => 'Xaçmaz',
        57 => 'Xocalı',
        58 => 'Xocavənd',
        59 => 'Xudat',
        60 => 'Xırdalan',
        61 => 'Xızı',
        62 => 'Yardımlı',
        63 => 'Yevlax',
        64 => 'Zaqatala',
        65 => 'Zəngilan',
        66 => 'Zərdab',
        67 => 'İmişli',
        68 => 'İsmayıllı',
        69 => 'Şabran',
        70 => 'Şahbuz',
        71 => 'Şamaxı',
        72 => 'Şirvan',
        73 => 'Şuşa',
        74 => 'Şəki',
        75 => 'Şəmkir',
        76 => 'Şərur',
    );

    private $colors = array(
        1 => 'Ağ',
        2 => 'Bej',
        3 => 'Boz',
        4 => 'Bənövşəyi',
        5 => 'Göy',
        6 => 'Gümüşü',
        7 => 'Mavi',
        8 => 'Narıncı',
        9 => 'Qara',
        10 => 'Qırmızı',
        11 => 'Qızılı',
        12 => 'Qəhvəyi',
        13 => 'Sarı',
        14 => 'Tünd qırmız',
        15 => 'Yaş Asfalt',
        16 => 'Yaşıl',
        17 => 'Çəhrayı',
    );

    /**
     * @param string $pageNum
     * @return array
     */
    public function getProducts($pageNum)
    {
        $start = !empty($pageNum) ? ($pageNum - 1) * $this->countProductEachPage : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*, `filemanager_mediafile`.`thumbs`
            FROM `products`
            LEFT JOIN `filemanager_mediafile`
            ON `filemanager_mediafile`.`id` = `products`.`photo_main_id`
            WHERE `moderation` = :moderation
            ORDER BY `products`.`date` DESC
            LIMIT :start, :limit
        ");

        $command->bindParam(':start', $start);
        $command->bindValue(':moderation', self::MODERATED);
        $command->bindParam(':limit', $this->countProductEachPage);

        $productData =  $command->query();
        $productData = $productData->readAll();

        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];
            $productData[$key]['productCategoryTypeName'] = $this->productCategoryType[$productData[$key]['product_category_type']];
            $photos = unserialize($options['thumbs']);

            if (empty($photos['medium'])) {
                continue;
            }

            $productData[$key]['photo_main'] = $photos['medium'];
        }

        return $productData;
    }

    /**
     * @param int $pageNum
     * @param int $carBrand
     * @param int $carModel
     * @param int $isNew
     * @return array|\yii\db\DataReader
     */
    public function getFiltration($pageNum, $carBrand, $carModel, $isNew)
    {
        $start = empty($pageNum) ? 0 : ($pageNum - 1) * $this->countProductEachPage;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*, `filemanager_mediafile`.`thumbs`
            FROM `products`
            LEFT JOIN `filemanager_mediafile`
            ON `filemanager_mediafile`.`id` = `products`.`photo_main_id`
            WHERE `moderation` = :moderation
            AND `car_brand` = :carBrand
            AND `car_model` = :carModel
            AND `new` = :isNew
            ORDER BY `products`.`date` DESC
            LIMIT :start, :limit
        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':carBrand', $carBrand);
        $command->bindValue(':carModel', $carModel);
        $command->bindValue(':isNew', $isNew);
        $command->bindValue(':start', $start);
        $command->bindValue(':limit', $this->countProductEachPage);

        $productData =  $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];

            $photos = unserialize($options['thumbs']);

            if (empty($photos['medium'])) {
                continue;
            }

            $productData[$key]['photo_main'] = $photos['medium'];
        }

        return $productData;
    }

    /**
     * @return int float
     */
    public function getPagesCount()
    {
        $connection = Yii::$app->db;
        $totalCountRows = $connection->createCommand('
            SELECT COUNT(*)
            FROM `products`
            WHERE `moderation` = 1
        ');

        $totalCountRows = $totalCountRows->queryScalar();
        $numPagination = ceil($totalCountRows / $this->countProductEachPage);

        return $numPagination;
    }

    /**
     * @return array
     */
    public function getLastProducts()
    {
        $productCount = self::LAST_PRODUCT_COUNT;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*, `filemanager_mediafile`.`thumbs`
            FROM `products`
            LEFT JOIN `filemanager_mediafile`
            ON `filemanager_mediafile`.`id` = `products`.`photo_main_id`
            WHERE `moderation` = 1
            ORDER BY `products`.`date` DESC
            LIMIT :productCount
        ");

        $command->bindParam(':productCount', $productCount);

        $productData =  $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $carBrand = (int) ArrayHelper::getValue($options, 'car_brand', 0);
            $productData[$key]['car_brand_name'] =  ArrayHelper::getValue($backendProductModel->carList, $carBrand . '.brand', '');
            $carModel = (int) ArrayHelper::getValue($options, 'car_model', 0);
            $productData[$key]['car_model_name'] =  ArrayHelper::getValue($backendProductModel->carList, $carBrand . '.model.' . $carModel);

            if (!isset($options['thumbs'])) {
                continue;
            }

            $photos = unserialize($options['thumbs']);
            $productData[$key]['photo_main'] = ArrayHelper::getValue($photos, 'medium', '');
        }

        return $productData;
    }
    
    /**
     * @return array
     */
    public function getBrand()
    {
        $productCount = self::LAST_PRODUCT_COUNT;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*, `filemanager_mediafile`.`thumbs`, COUNT(`car_brand`) as `car_brand_count`
            FROM `products`
            LEFT JOIN `filemanager_mediafile`
            ON `filemanager_mediafile`.`id` = `products`.`photo_main_id`
            WHERE `moderation` = 1
            GROUP BY `car_brand`
            ORDER BY `products`.`date` DESC
            LIMIT :productCount
        ");

        $command->bindParam(':productCount', $productCount);

        $productDataBrand =  $command->query();
        $productDataBrand = $productDataBrand->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productDataBrand as $key => $options) {
            $carBrand = (int) ArrayHelper::getValue($options, 'car_brand', 0);
            $productDataBrand[$key]['car_brand_name'] =  ArrayHelper::getValue($backendProductModel->carList, $carBrand . '.brand', '');
            $carModel = (int) ArrayHelper::getValue($options, 'car_model', 0);
            $productDataBrand[$key]['car_model_name'] =  ArrayHelper::getValue($backendProductModel->carList, $carBrand . '.model.' . $carModel);

            if (!isset($options['thumbs'])) {
                continue;
            }

            $photos = unserialize($options['thumbs']);
            $productDataBrand[$key]['photo_main'] = ArrayHelper::getValue($photos, 'medium', '');
        }

        return $productDataBrand;
    }

    /**
     * @param int $productId
     * @return array|bool
     */
    public function getProduct($productId)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT *
            FROM `products`
            WHERE `product_id` = :product_id AND `moderation` = 1
        ");

        $command->bindValue(':product_id', $productId);
        $productData = $command->queryOne();
        $backendProductModel = new BackendProduct();

        if (empty($productData)) {
            return array();
        }

        $productData['car_brand_name'] = $backendProductModel->carList[$productData['car_brand']]['brand'];
        $productData['car_model_name'] = $backendProductModel->carList[$productData['car_brand']]['model'][$productData['car_model']];
        $productData['color_name'] = $this->colors[$productData['color']];
        $productData['city_name'] = $this->cities[$productData['city']];
        $productData['production_country_name'] = $this->countries[$productData['production_country']];
        $productData['productCategoryTypeName'] = $this->productCategoryType[$productData['product_category_type']];

        $photoIds = unserialize($productData['photo_ids']);

        $query = new Query;
        $query
            ->select(['id', 'thumbs'])
            ->from('filemanager_mediafile')
            ->where(array('in', 'id', $photoIds));
        $command = $query->createCommand();
        $photos = $command->queryAll();

        foreach($photos as $id => $photo) {
            $photos[$id] =  unserialize($photo['thumbs']);
        }

        $productData['photos'] = $photos;

        return $productData;
    }


    public function addProduct($productName, $manufacturer, $serialNumber, $carBrand, $carModel, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city, $sellerName, $sellerMail, $sellerPhone1, $sellerPhone2, $new, $utilized, $photoMainId,  $photoIds, $moderation, $date)
    {

        $connection = Yii::$app->db;

        $command = $connection->createCommand('
            INSERT `products`
            SET `product_name`       = :productName,
                `manufacturer`       = :manufacturer,
                `serial_number`      = :serialNumber,
                `car_brand`          = :carBrand,
                `car_model`          = :carModel,
                `production_year`    = :productionYear,
                `production_country` = :productionCountry,
                `for_year_min`       = :forYearMin,
                `for_year_max`       = :forYearMax,
                `mileage`            = :mileage,
                `color`              = :color,
                `price`              = :price,
                `delivery`           = :delivery,
                `setup`              = :setup,
                `editor`             = :editor,
                `guarantee`          = :guarantee,
                `credit`             = :credit,
                `barter`             = :barter,
                `perfect`            = :perfect,
                `average`            = :average,
                `notwork`            = :notwork,
                `product_category_type` = :productCategoryType,
                `city`               = :city,
                `seller_name`        = :sellerName,
                `seller_mail`        = :sellerMail,
                `seller_phone1`      = :sellerPhone1,
                `seller_phone2`      = :sellerPhone2,
                `new`                = :new,
                `utilized`           = :utilized,
                `photo_main_id`      = :photoMainId,
                `photo_ids`          = :photoIds,
                `moderation`         = :moderation,
                `date`               = :date
        ');

        $command->bindValues(array(
            ':productName'       => $productName,
            ':manufacturer'      => $manufacturer,
            ':serialNumber'      => $serialNumber,
            ':carBrand'          => $carBrand,
            ':carModel'          => $carModel,
            ':productionYear'    => $productionYear,
            ':productionCountry' => $productionCountry,
            ':forYearMin'        => $forYearMin,
            ':forYearMax'        => $forYearMax,
            ':mileage'           => $mileage,
            ':color'             => $color,
            ':price'             => $price,
            ':delivery'          => $delivery,
            ':setup'             => $setup,
            ':editor'            => $editor,
            ':guarantee'         => $guarantee,
            ':credit'            => $credit,
            ':barter'            => $barter,
            ':perfect'           => $perfect,
            ':average'           => $average,
            ':notwork'           => $notwork,
            ':productCategoryType' => $productCategoryType,
            ':city'              => $city,
            ':sellerName'        => $sellerName,
            ':sellerMail'        => $sellerMail,
            ':sellerPhone1'      => $sellerPhone1,
            ':sellerPhone2'      => $sellerPhone2,
            ':new'               => $new,
            ':utilized'          => $utilized,
            ':photoMainId'       => $photoMainId,
            ':photoIds'          => $photoIds,
            ':moderation'        => $moderation,
            ':date'              => $date,
        ));

        $command->execute();

        return true;
    }

    /**
     * @return array
     */
    public function getCountriesList()
    {
        return $countries = $this->countries;
    }

    /**
     * @return array
     */
    public function getColorsList()
    {
        return $colors = $this->colors;
    }

    /**
     * @return array
     */
    public function getCitiesList()
    {
        return $cities = $this->cities;
    }

    /**
     * @return array
     */
    public function getGeneralProductsCountByCarBrand()
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `product_id`, `car_brand`, `car_model`, COUNT(`car_brand`) as `car_brand_count`
            FROM `products`
            WHERE `moderation` = :moderation
            AND `product_category_type` = :productCategoryType
            GROUP BY `car_brand`
        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':productCategoryType', self::PRODUCT_TYPE_GENERAL);

        $productData = $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];
        }

        return $productData;
    }

    /**
     * @return array
     */
    public function getProductsCountByCarModel($productId)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `car_model`, `car_brand`, COUNT(`car_model`) as `car_model_count`
            FROM `products`
            WHERE `moderation` = :moderation
            AND `product_category_type` = :productCategoryType
            AND `car_brand` = :productId
            GROUP BY `car_model`
        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':productCategoryType', self::PRODUCT_TYPE_GENERAL);
        $command->bindValue(':productId', $productId);

        $productData = $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] = $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =    $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];
        }

        return $productData;
    }

    /**
     * @return array
     */
    public function getModelList($productId)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `car_model`, `car_brand`, COUNT(`car_model`) as `car_model_count`
            FROM `products`
            WHERE `moderation` = :moderation
            AND `car_brand` = :productId
            GROUP BY `car_model`
        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':productId', $productId);

        $productData = $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];
        }

        return $productData;
    }

    /**
     * @return array
     */
    public function getCarModelProduct($dataCarBrand, $dataCarModel, $pageNum)
    {
        $start = !empty($pageNum) ? ($pageNum - 1) * $this->countProductEachPage : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*, `filemanager_mediafile`.`thumbs`
            FROM `products`
            LEFT JOIN `filemanager_mediafile`
            ON `filemanager_mediafile`.`id` = `products`.`photo_main_id`
            WHERE `moderation` = :moderation
            AND `product_category_type` = :productCategoryType
            AND `car_brand` = :dataCarBrand
            AND `car_model` = :dataCarModel
            ORDER BY `products`.`date` DESC
            LIMIT :start, :limit
        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':productCategoryType', self::PRODUCT_TYPE_GENERAL);
        $command->bindParam(':dataCarBrand', $dataCarBrand);
        $command->bindParam(':dataCarModel', $dataCarModel);
        $command->bindParam(':start', $start);
        $command->bindParam(':limit', $this->countProductEachPage);

        $productData =  $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];

            $photos = unserialize($options['thumbs']);

            if (empty($photos['medium'])) {
                continue;
            }

            $productData[$key]['photo_main'] = $photos['medium'];
        }


        return $productData;
    }

    /**
     * @param int $dataCarBrand
     * @param int $dataCarModel
     * @return array
     */
    public function getCarModelProductsFool($dataCarBrand, $dataCarModel)
    {
       // $start = !empty($pageNum) ? ($pageNum - 1) * $this->countProductEachPage : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*
            FROM `products`
            WHERE `moderation` = :moderation
            AND `product_category_type` = :productCategoryType
            AND `car_brand` = :dataCarBrand
            AND `car_model` = :dataCarModel
            ORDER BY `products`.`date` DESC

        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':productCategoryType', self::PRODUCT_TYPE_GENERAL);
        $command->bindValue(':dataCarBrand', $dataCarBrand);
        $command->bindValue(':dataCarModel', $dataCarModel);



        $productData = $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];
        }

        return $productData;
    }

    /**
     * @param int $dataCarBrand
     * @param int $dataCarModel
     * @return array
     */
    public function getProductsByModel($dataCarBrand, $dataCarModel)
    {
       // $start = !empty($pageNum) ? ($pageNum - 1) * $this->countProductEachPage : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*
            FROM `products`
            WHERE `moderation` = :moderation
            AND `car_brand` = :dataCarBrand
            AND `car_model` = :dataCarModel
            ORDER BY `products`.`date` DESC

        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':dataCarBrand', $dataCarBrand);
        $command->bindValue(':dataCarModel', $dataCarModel);

        $productData = $command->query();
        $productData = $productData->readAll();
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];
        }

        return $productData;
    }

    /**
     * @param int $pageNum
     * @param int $carBrand
     * @param int $carModel
     * @param int $isNew
     * @param int $productionYear
     * @param int $productionCountry
     * @param int $forYearMin
     * @param int $forYearMax
     * @param int $mileage
     * @param int $color
     * @param int $price
     * @param int $delivery
     * @param int $setup
     * @param int $editor
     * @param int $guarantee
     * @param int $credit
     * @param int $barter
     * @param int $perfect
     * @param int $average
     * @param int $notwork
     * @param int $productCategoryType
     * @param int $city
     * @return array|\yii\db\DataReader
     */
    public function getSearch($pageNum, $carBrand, $carModel, $isNew, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city)
    {
        $start = empty($pageNum) ? 0 : ($pageNum - 1) * $this->countProductEachPage;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
            SELECT `products`.*, `filemanager_mediafile`.`thumbs`
            FROM `products`
            LEFT JOIN `filemanager_mediafile`
            ON `filemanager_mediafile`.`id` = `products`.`photo_main_id`
            WHERE `moderation` = :moderation
            AND `car_brand` = :carBrand
            AND `car_model` = :carModel
            AND `production_year` = :productionYear
            AND `production_country` = :productionCountry
            AND `for_year_min` = :forYearMin
            AND `for_year_max` = :forYearMax
            AND `mileage` <= :mileage
            AND `color` = :color
            AND `price` <= :price
            AND `delivery` = :delivery
            AND `setup` = :setup
            AND `editor` = :editor
            AND `guarantee` = :guarantee
            AND `credit` = :credit
            AND `barter` = :barter
            AND `perfect` = :perfect
            AND `average` = :average
            AND `notwork` = :notwork
            AND `product_category_type` = :productCategoryType
            AND `city` = :city
            AND `new` = :isNew
            ORDER BY `products`.`date` DESC
            LIMIT :start, :limit
        ");

        $command->bindValue(':moderation', self::MODERATED);
        $command->bindValue(':carBrand', $carBrand);
        $command->bindValue(':carModel', $carModel);
        $command->bindValue(':productionYear', $productionYear);
        $command->bindValue(':productionCountry', $productionCountry);
        $command->bindValue(':forYearMin', $forYearMin);
        $command->bindValue(':forYearMax', $forYearMax);
        $command->bindValue(':mileage', $mileage);
        $command->bindValue(':color', $color);
        $command->bindValue(':price', $price);
        $command->bindValue(':delivery', $delivery);
        $command->bindValue(':setup', $setup);
        $command->bindValue(':editor', $editor);
        $command->bindValue(':guarantee', $guarantee);
        $command->bindValue(':credit', $credit);
        $command->bindValue(':barter', $barter);
        $command->bindValue(':perfect', $perfect);
        $command->bindValue(':average', $average);
        $command->bindValue(':notwork', $notwork);
        $command->bindValue(':productCategoryType', $productCategoryType);
        $command->bindValue(':city', $city);
        $command->bindValue(':isNew', $isNew);
        $command->bindValue(':start', $start);
        $command->bindValue(':limit', $this->countProductEachPage);

        $productData =  $command->query();
        $productData = $productData->readAll();
        $productData['productCategoryTypeName'] = $this->productCategoryType[$productData['product_category_type']];
        $backendProductModel = new BackendProduct();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $backendProductModel->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $backendProductModel->carList[$options['car_brand']]['model'][$options['car_model']];

            $photos = unserialize($options['thumbs']);

            if (empty($photos['medium'])) {
                continue;
            }

            $productData[$key]['photo_main'] = $photos['medium'];
        }

        return $productData;
    }

    public function sendMessage($sellerMail)
    {
        Yii::$app->mailer->compose('contact/html.php', ['contactForm' => 'no-reply@oluxana.az'])
            ->setFrom('no-reply@oluxana.az')
            ->setTo($sellerMail)
            ->setSubject('Oluxana bildiriş')
            ->send();

        return true;
    }

    /**
     * @return array
     */
    public function getproductCategoryType()
    {
        return $countries = $this->productCategoryType;
    }
}
