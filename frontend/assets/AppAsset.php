<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/html5reset.css',
        'css/common.css',
        'css/product.css',
        'css/repairer.css',
        'css/declaration.css',
    ];
    public $js = [
        'js/jquery.screwdefaultbuttonsV2.min.js',
        'js/notifyjs-v0.3.1.js',
        'js/jquery.validate.js',
        'js/products.js',
        'js/declarations.js',
        'js/repairers.js',
        'js/search.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
