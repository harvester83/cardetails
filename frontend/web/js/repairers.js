$(document).ready(function() {
    $('#pagination-repairer').on('click', 'li', function() {
        var data = {
            pageNum: $(this).index()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/repairers/list',
            data: data,
            success: function(data) {
                $('.repairers-list').html(data);
            }
        });

        return false;
    });
});