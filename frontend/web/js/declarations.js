$(document).ready(function() {
    // VALIDATION
    $('#add-declaration-form').validate({
        rules: {
            productName: "required",
            buyer: "required",
            email: "required",
            phone: "required"
        },
        messages: {
            productName: "* Hissənin adını əlavə edin",
            buyer: "* Adınızı qeyt edin",
            email: "* Elektron ünvanınızı qeyd edin",
            phone: "* Əlaqə nömrənizi qeyd edin"
        },
        submitHandler: function() {
            var declarationData = {
                productName: $('#product-name').val(),
                editor: $('#editor').html(),
                buyer: $('#buyer').val(),
                email: $('#email').val(),
                phone: $('#phone').val()
            };

            $.ajax({
                type: "POST",
                url: absoluteUrl + '/declarations/send',
                data: declarationData,

                success: function(data) {
                    data = JSON.parse(data);

                    if (!data.result) {
                        $.notify('Elan dərc olunmadı. Zəhmət olmasa məlumatları bir daha yoxlayıb göndərəsiz', 'error');

                        return false;
                    }

                    $.notify('Təşəkkür edirik elanıvız dərc olunması üçün moderatora göndərildi.', 'success');
                }
            });

            return false;
        }
    });

    $('#pagination-declaration').on('click', 'li', function() {
        var data = {
            pageNum: $(this).index()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/declarations/list',
            data: data,
            success: function(data) {
                $('.category').html(data);
            }
        });

        return false;
    });
});

