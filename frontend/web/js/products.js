$(document).ready(function() {
    //change cars model
    $('#car-brand').on('change', function () {
        var carBrandId = $(this).val();
        var carModels = carList[carBrandId]['model'];

        var $carModelWrapper = $('#car-model');
        $carModelWrapper.empty();

        $.each(carModels, function(modelId, carModelName) {
            $($carModelWrapper).append('<option value=" ' + modelId + ' ">' + carModelName + '</option>');
        });
    });

    //change cars model
    $('#car-brand-filtration').on('change', function () {
        var carBrandId = $(this).val();
        var carModels = carList[carBrandId]['model'];

        var $carModelWrapper = $('#car-model-filtration');
        $carModelWrapper.empty();

        $.each(carModels, function(modelId, carModelName) {
            $($carModelWrapper).append('<option value=" ' + modelId + ' ">' + carModelName + '</option>');
        });
    });

    for(var year = 2015; year >= 1955; year--) {
        $('select#production-year, select#for-year-min, select#for-year-max').append('<option value="' + year + '">' + year + '</option>');
    }

    var productPhotoIds = [];

    $('#mediafile-file-form')
        .bind('fileuploadstarted', function() {
            $('.send-product').attr('disabled', true);
        })
        .bind('fileuploadfinished', function(e, data) { //собираем все загруженные фотки
            var file = data.result.files[0].deleteUrl;
            var fileData = file.split('=');
            productPhotoIds.push(fileData[1]);
        })
        .bind('fileuploadstopped', function() {
            $('.send-product').removeAttr('disabled');
        });

    $('#add-product-form').validate({

        rules: {
            productName: "required",
            serialNumber: "required",
            price: "required",
            sellerName: "required",
            sellerMail: "required",
            sellerPhone: "required"

        },

        messages: {
            productName: "Hissənin adını qeyd edin",
            serialNumber: "Hissənin seriya nömrəsini qeyd edin",
            price: "Qiyməti qeyd edin",
            sellerName: "Adınızı qeyd edin",
            sellerMail: "Email ünvanınızı qeyd edin",
            sellerPhone: "Əlaqə nömrəvnizi qeyd edin"
        },

        submitHandler: function() {
            var photoMainId = productPhotoIds[0];
            if (!photoMainId) {
                photoMainId = 0;
            }

            var productCategoryType = $('input:radio[name=check-button-category]:checked').val();

            if ($('#action-part').prop('checked')) {
                productCategoryType = $('#action-part').val();
            }

            var selected = 1;

            var credit = 0;
            if ($('#credit').prop('checked')) {
                credit = selected;
            }
            var delivery = 0;
            if ($('#delivery').prop('checked')) {
                 delivery = selected;
            }

            var setup = 0;
            if ($('#setup').prop('checked')) {
                 setup = selected;
            }

            var guarantee = 0;
            if ($('#guarantee').prop('checked')) {
                guarantee = selected;
            }

            var barter = 0;
            if ($('#barter').prop('checked')) {
                barter = selected;
            }

            var perfect = 0;
            if ($('#perfect').prop('checked')) {
                 perfect = selected;
            }

            var average = 0;
            if ($('#average').prop('checked')) {
                 average = selected;
            }

            var notwork = 0;
            if ($('#notwork').prop('checked')) {
                notwork = selected;
            }

            var utilizedDetail = 0;
            if ($('#utilized-detail').prop('checked')) {
                utilizedDetail = selected;
            }

            var newDetail = 0;
            if ($('#new-detail').prop('checked')) {
                 newDetail = selected;
            }

            var productData = {
                productName:       $('#product-name').val(),
                manufacturer:      $('#manufacturer').val(),
                serialNumber:      $('#serial-number').val(),
                carBrand:          $('select#car-brand option:selected').val(),
                carModel:          $('#car-model-special #car-model option:selected').val(),
                productionYear:    $('#production-year option:selected').val(),
                productionCountry: $('#production-country option:selected').val(),
                forYearMin:        $('#for-year-min option:selected').val(),
                forYearMax:        $('#for-year-max option:selected').val(),
                mileage:           $('#mileage').val(),
                color:             $('#color option:selected').val(),
                price:             $('#price').val(),
                delivery:          delivery,
                setup:             setup,
                editor:            $('#editor').val(),
                guarantee:         guarantee,
                credit:            credit,
                barter:            barter,
                perfect:           perfect,
                average:           average,
                notwork:           notwork,
                productCategoryType: productCategoryType,
                city:              $('#city option:selected').val(),
                sellerName:        $('#seller-name').val(),
                sellerMail:        $('#seller-mail').val(),
                sellerPhone1:      $('#seller-phone1').val(),
                sellerPhone2:      $('#seller-phone2').val(),
                newDetail:         newDetail,
                utilizedDetail:    utilizedDetail,
                photoMainId:       photoMainId,
                photoIds:          productPhotoIds
            };

            $.ajax({
                type: "POST",
                url: absoluteUrl + '/products/send',
                data: productData,
                success: function(data) {

                    data = JSON.parse(data);

                    if (!data.result) {
                        $.notify('Elan dərc olunmadı. Zəhmət olmasa məlumatları bir daha yoxlayıb göndərəsiz', 'error');

                        return false;
                    }

                    $.notify('Təşəkkür edirik elanıvız dərc olunması üçün moderatora göndərildi.', 'success');
                }
            });

            return false;
        }
    });

    $('button.filtration-submit').on('click', function() {
        var isNew = 0;
        if ($('#new').prop('checked')) {
            isNew = 1;
        }

        var productData = {
            carBrand: $('#car-brand-filtration option:selected').val(),
            carModel: $('#car-model-filtration option:selected').val(),
            isNew: isNew
        };

        /*console.log(productData);
        return false;*/

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/filtration',
            data: productData,
            success: function(data) {
                $('.wrap-category').html(data)
            }
        });

        return false;
    });

    $('#pagination-products').on('click', 'li', function() {
        $('.pagination li').removeClass('active-page');
        $(this).addClass('active-page');
        console.log($(this).index());
        var data = {
            pageNum: $(this).index()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/list',
            data: data,
            success: function(data) {
                $('.category').html(data);
            }
        });

        return false;
    });

    $('#pagination-products .pagination-prew').on('click', function() {
        var activeItemIndex = $('.active-page').index();
        var pageNum = activeItemIndex - 1;
        $('.pagination li').removeClass('active-page');
        $('.pagination li').eq(pageNum).addClass('active-page');
        console.log($(this).index());
        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/list',
            data: pageNum,
            success: function(data) {
                $('.category').html(data);
            }
        });

        return false;
    });

    $('#pagination-products .pagination-next').on('click', function() {
        var activeItemIndex = $('.active-page').index();
        var pageNum = activeItemIndex + 1;
        $('.pagination li').removeClass('active-page');
        $('.pagination li').eq(pageNum).addClass('active-page');
        console.log($(this).index());
        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/list',
            data: pageNum,
            success: function(data) {
                $('.category').html(data);
            }
        });

        return false;
    });

    $('.all-search').on('click', function() {
        var isNew = 0;
        if ($('#new-search').prop('checked')) {
            isNew = 1;
        }

        var delivery = 0;
        if ($('#delivery').prop('checked')) {
            delivery = 1;
        }

        var setup = 0;
        if ($('#setup').prop('checked')) {
            setup = 1;
        }

        var guarantee = 0;
        if ($('#guarantee').prop('checked')) {
            guarantee = 1;
        }

        var credit = 0;
        if ($('#credit').prop('checked')) {
           credit = 1;
        }

        var barter = 0;
        if ($('#barter').prop('checked')) {
            barter = 1
        }

        var perfect = 0;
        if ($('#perfect').prop('checked')) {
            perfect = 1
        }

        var average = 0;
        if ($('#average').prop('checked')) {
            average = 1
        }

        var notwork = 0;
        if ($('#notwork').prop('checked')) {
            notwork = 1
        }

        var indexOfGeneralCat = 4;
        var productCategoryType = indexOfGeneralCat;
        if ($('#interior').prop('checked')) {
            productCategoryType = $('#interior').val();
        }

        if ($('#exterior').prop('checked')) {
            productCategoryType = $('#exterior').val();
        }

        if ($('#general').prop('checked')) {
            productCategoryType = $('#general').val();
        }

        if ($('#action-part').prop('checked')) {
            productCategoryType = $('#action-part').val();
        }

        var productData = {
            carBrand:          $('#car-brand option:selected').val(),
            carModel:          $('#car-model option:selected').val(),
            productionYear:    $('#production-year option:selected').val(),
            productionCountry: $('#production-country option:selected').val(),
            forYearMin:        $('#for-year-min option:selected').val(),
            forYearMax:        $('#for-year-max option:selected').val(),
            mileage:           $('#mileage').val(),
            color:             $('#color option:selected').val(),
            price:             $('#price').val(),
            delivery:          delivery,
            setup:             setup,
            guarantee:         guarantee,
            credit:            credit,
            barter:            barter,
            perfect:           perfect,
            average:           average,
            notwork:           notwork,
            productCategoryType: productCategoryType,
            isNew:             isNew,
            city:              $('#city option:selected').val(),
        };

        console.log(productData);

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/search',
            data: productData,
            success: function(data) {
                $('.wrap-category').html(data)
            }
        });

        return false;
    });

    //Appeal ad
    $('.button-action').on('click', function() {
        $('.block-appeal').slideToggle();
    });

    $('.car-model-item').on('click', function() {
       var productData =  {
            dataCarBrand: $(this).attr('data-car-brand'),
            dataCarModel: $(this).attr('data-car-model')
       };

       $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/modelproducts',
            data: productData,
            success: function(data) {
                $('.wrap-category').html(data)
            }
       });

        return false
    });

    $('.car-model-list').on('click', function() {
        var productData =  {
            dataCarBrand: $(this).attr('data-car-brand'),
            dataCarModel: $(this).attr('data-car-model')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/getproductsbymodel',
            data: productData,
            success: function(data) {
                $('.wrap-category').html(data)
            }
        });

        return false
    });

    $('#general-search-button').on('click', function () {
        var searchData =  {
            productId: $('.general-search-field').val()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/searchbyid',
            data: searchData,
            success: function(data) {
                $('.wrap-category').html(data)
            }
        });

        return false
    });
});