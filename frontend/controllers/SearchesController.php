<?php

namespace frontend\controllers;

use backend\models\Product as BackendProduct;
use frontend\models\Product;
use Imagine\Exception\Exception;
use Yii;
use yii\web\Controller;

class SearchesController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $products = array();
        $pageNum = 0;
        $pagesCount = (int)$this->getModel()->getPagesCount();
        $countries = (array) $this->getModel()->getCountriesList();
        $colors = (array) $this->getModel()->getColorsList();
        $cities = (array) $this->getModel()->getCitiesList();

        $backendProductModel = new BackendProduct();
        $carList = $backendProductModel->getCarList();


        try {
            $products = $this->getModel()->getProducts($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('index', ['products' => $products,
            'pagesCount' => $pagesCount,
            'cities' => $cities,
            'colors' => $colors,
            'countries' => $countries,
            'carList' => $carList,
        ]);
    }

    /**
     * @return Product
     */
    public function getModel()
    {
        return new Product();
    }
}