<?php

namespace frontend\controllers;

//use backend\controllers\SiteController;
use Exception;
use frontend\models\Repairer;
use Yii;
use yii\web\Controller;

class RepairersController extends Controller
{
    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $posts = array();
        $Repairer = new Repairer();
        $pageNum = 0;
        $pagesCount = (int)$Repairer->getPagesCount();

        try {
            $posts = $Repairer->getRepairers($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('index.php', ['posts' => $posts, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $request = Yii::$app->request;
        $Repairer = new Repairer();
        $pageNum = (int) $request->post('pageNum', 0);
        $posts = array();

        try {
            $posts = $Repairer->getRepairers($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('repairers.php', ['posts' => $posts]);
    }

}
