<?php

namespace frontend\controllers;

use frontend\models\Declaration;
use Exception;
use Yii;
use yii\web\Controller;

class DeclarationsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $pageNum = $request->post('pageNum', 0);
        $declarationData = [];
        $pagesCount = 0;

        try {
            $pagesCount = $this->getModel()->getpagesCount();
            $declarationData = (array)$this->getModel()->getDeclarations($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('index', [
            'declarations' => $declarationData,
            'pagesCount' => $pagesCount
        ]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $request = Yii::$app->request;
        $pageNum = (int) $request->post('pageNum', 0);

        $declarations = array();

        try {
            $declarations = $this->getModel()->getDeclarations($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('declarations.php', ['declarations' => $declarations]);
    }

    /**
     * @return bool
     */
    public function actionAdd()
    {
        return $this->render('add-declaration');
    }

    /**
     * @return bool
     */
    public function actionSend()
    {
        $request = Yii::$app->request;
        $productName = $request->post('productName');
        $editor = $request->post('editor');
        $buyer = $request->post('buyer');
        $email = $request->post('email');
        $phone = $request->post('phone');
        $moderation = 0;
        $date = date('Y-m-d H:i');

        $declarationData = [];

        try {
            $declarationData = (array)$this->getModel()->sendDeclaration($productName, $editor, $buyer, $email, $phone, $moderation, $date);
            $result = true;
        } catch (Exception $e) {
            $result = false;

        }

        $declaration = array(
            'result'      => $result,
            'declaration' => $declarationData,
        );

        echo json_encode($declaration);
    }

    /**
     * @return Declaration
     */
    public function getModel()
    {
        return new Declaration();
    }

}