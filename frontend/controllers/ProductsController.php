<?php

namespace frontend\controllers;

use backend\models\Product as BackendProduct;
use Exception;
use frontend\models\Product;
use pendalf89\filemanager\models\Mediafile;
use Yii;
use yii\web\Controller;

class ProductsController extends Controller
{
    /**
     * @var boolean whether to enable CSRF validation for the actions in this controller.
     * CSRF validation is enabled only when both this property and [[Request::enableCsrfValidation]] are true.
     */
    public $enableCsrfValidation = false;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $products = array();
        $pageNum = 0;
        $pagesCount = (int) $this->getModel()->getPagesCount();

        try {
            $products = $this->getModel()->getProducts($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('index', ['products' => $products, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionProduct()
    {
        $request = Yii::$app->request;
        $productId = (int)$request->get('id');
        $productData = array();

        try {
            $productData = $this->getModel()->getProduct($productId);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('product', ['product' => $productData]);
    }

    /**
     * @return string
     */
    public function actionGeneralProductsList()
    {
        $products = array();

        try {
            $products = $this->getModel()->getGeneralProductsCountByCarBrand();
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('general', ['products' => $products]);
    }

    /**
     * @return string
     */
    public function actionModellist()
    {
        $request = Yii::$app->request;
        $productId = (int)$request->get('id');

        $products = array();
        $pagesCount = (int) $this->getModel()->getPagesCount();

        try {
            $products = $this->getModel()->getProductsCountByCarModel($productId);
        } catch (Exception $e) {
         Yii::error($e);
        }

        return $this->render('models', ['products' => $products, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionGetmodellist()
    {
        $request = Yii::$app->request;
        $productId = (int)$request->get('id');

        $products = array();
        $pagesCount = (int) $this->getModel()->getPagesCount();

        try {
            $products = $this->getModel()->getModelList($productId);
        } catch (Exception $e) {
         Yii::error($e);
        }

        return $this->render('model-list-parts', ['products' => $products, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionModelproducts()
    {
        $request = Yii::$app->request;
        $dataCarBrand =  (int)$request->post('dataCarBrand');
        $dataCarModel =  (int)$request->post('dataCarModel');
      //  $pageNum = 0;
       // $pagesCount = (int) $this->getModel()->getPagesCount();
        $products = [];

        try {
            $products = $this->getModel()->getCarModelProductsFool($dataCarBrand, $dataCarModel);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('products', ['products' => $products]);
    }

    /**
     * @return string
     */
    public function actionGetproductsbymodel()
    {
        $request = Yii::$app->request;
        $dataCarBrand =  (int)$request->post('dataCarBrand');
        $dataCarModel =  (int)$request->post('dataCarModel');
      //  $pageNum = 0;
       // $pagesCount = (int) $this->getModel()->getPagesCount();
        $products = [];

        try {
            $products = $this->getModel()->getProductsByModel($dataCarBrand, $dataCarModel);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('products', ['products' => $products]);
    }

    /**
     * @return string
     */
    public function actionFiltration()
    {
        $request    = Yii::$app->request;
        $carBrand   = (int) $request->post('carBrand');
        $carModel   = (int) $request->post('carModel');
        $isNew      = (int) $request->post('isNew');
        $pageNum    = (int) $request->post('pageNum', 0);
        $products   = array();

        $pagesCount = (int) $this->getModel()->getPagesCount();

        try {
            $products = $this->getModel()->getFiltration($pageNum, $carBrand, $carModel, $isNew);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('filtration', ['products' => $products, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $carBrand = (int)$request->post('carBrand');
        $carModel = (int)$request->post('carModel');
        $productionYear = (int)$request->post('productionYear');
        $productionCountry = (int)$request->post('productionCountry');
        $forYearMin = (int)$request->post('forYearMin');
        $forYearMax = (int)$request->post('forYearMax');
        $mileage = (int)$request->post('mileage', 0);
        $color = (int)$request->post('color');
        $price = (int)$request->post('price', 0);
        $delivery = (int)$request->post('delivery');
        $setup = (int)$request->post('setup');
        $editor = (int)$request->post('editor');
        $guarantee = (int)$request->post('guarantee');
        $credit = (int)$request->post('credit');
        $barter = (int)$request->post('barter');
        $perfect = (int)$request->post('perfect');
        $average = (int)$request->post('average');
        $notwork = (int)$request->post('notwork');
        $productCategoryType = (int)$request->post('productCategoryType');
        $city = (int)$request->post('city');
        $isNew = (int)$request->post('isNew');
        $pageNum = (int)$request->post('pageNum', 0);
        $products = array();
        $pagesCount = (int)$this->getModel()->getPagesCount();

        try {
            $products = $this->getModel()->getSearch($pageNum, $carBrand, $carModel, $isNew, $productionYear, $productionCountry,$forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city
            );
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('filtration', ['products' => $products, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionAdd()
    {
        $countries = (array) $this->getModel()->getCountriesList();
        $colors = (array) $this->getModel()->getColorsList();
        $cities = (array) $this->getModel()->getCitiesList();

        $backendProductModel = new BackendProduct();
        $carList = $backendProductModel->getCarList();
        $model = new Mediafile();

        return $this->render('add-product', [
            'model' => $model,
            'countries' => $countries,
            'cities' => $cities,
            'colors' => $colors,
            'carList' => $carList,
        ]);
    }

    /**
     * @return string
     */
    public function actionSend()
    {
        $request = Yii::$app->request;
        $productName        = $request->post('productName');
        $manufacturer       = $request->post('manufacturer');
        $serialNumber       = $request->post('serialNumber');
        $carBrand           = (int) $request->post('carBrand');
        $carModel           = (int) $request->post('carModel');
        $productionYear     = (int) $request->post('productionYear');
        $productionCountry  = (int) $request->post('productionCountry');
        $forYearMin         = (int) $request->post('forYearMin');
        $forYearMax         = (int) $request->post('forYearMax');
        $mileage            = (int) $request->post('mileage');
        $color              = (int) $request->post('color');
        $price              = (int) $request->post('price');
        $delivery           = (int) $request->post('delivery');
        $setup              = (int) $request->post('setup');
        $editor             =  $request->post('editor');
        $guarantee          = (int) $request->post('guarantee');
        $credit             = (int) $request->post('credit');
        $barter             = (int) $request->post('barter');
        $perfect            = (int) $request->post('perfect');
        $average            = (int) $request->post('average');
        $notwork            = (int) $request->post('notwork');
        $productCategoryType = (int) $request->post('productCategoryType');
        $city               = (int) $request->post('city');
        $sellerName         = $request->post('sellerName');
        $sellerMail         = $request->post('sellerMail');
        $sellerPhone1       = $request->post('sellerPhone1');
        $sellerPhone2       = $request->post('sellerPhone2');
        $new                = (int) $request->post('newDetail');
        $utilized           = (int) $request->post('utilizedDetail');
        $photoMainId = $request->post('photoMainId');
        $photoIds = serialize($request->post('photoIds'));
        $moderation         = 0;
        $date               = date('Y-m-d H:i');

        if (!$sellerMail || !$productName || !$sellerPhone1) {
            echo json_encode(array(
               'result' => false,
            ));

            return false;
        }

        $productData = array();
        try {
            $productData = (array) $this->getModel()->addProduct($productName, $manufacturer, $serialNumber, $carBrand, $carModel, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city, $sellerName, $sellerMail, $sellerPhone1, $sellerPhone2, $new, $utilized, $photoMainId,  $photoIds, $moderation, $date);

            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        try {
            $this->getModel()->sendMessage($sellerMail);
        } catch (Exception $e) {

        }

        $product = array(
            'result' => $result,
            'postData' => $productData
        );

        echo json_encode($product);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $request = Yii::$app->request;
        $pageNum = (int) $request->post('pageNum', 0);
        $products = array();

        try {
            $products = $this->getModel()->getProducts($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('products.php', ['products' => $products]);
    }

    /**
     * @return string
     */
    public function actionSearchbyid()
    {
        $request = Yii::$app->request;
        $productId = (int)$request->post('productId');
        $productData = [];

        try {
            $productData = $this->getModel()->getProduct($productId);
        } catch (Exception $e) {
            Yii::error($e);
        }

        if (empty($productData)) {
            return $this->renderPartial('error', ['product' => $productData]);
        }

        return $this->renderPartial('product', ['product' => $productData]);
    }

    /**
     * @return  Product
     */
    public function getModel()
    {
        return new Product();
    }
}
