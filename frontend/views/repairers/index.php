<div class="wrap-category repairer">
    <h2 class="block-title-repairer">Ustalar</h2>
    <div class="repairers-list">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Texniki sahəsi</th>
                    <th>Soyadı Adı</th>
                    <th>Ünvan</th>
                    <th>Tel</th>
                </tr>
            </thead>

            <?php foreach ($posts as $item) { ?>
                <tbody>
                    <tr>
                        <td><?= $item['profile']; ?></td>
                        <td><?= $item['full_name']; ?></td>
                        <td><?= $item['address']; ?></td>
                        <td><?= $item['contact_num']; ?></td>
                    </tr>
                </tbody>
            <?php } ?>

        </table>
    </div>
    <nav class="pagination-wrap" id="pagination-repairer">
        <ul class="pagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>

            <?php for($i =1; $i <= $pagesCount; $i++){ ?>
                <li><a href="#"><?php echo $i; ?></a></li>
            <?php }; ?>

            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>