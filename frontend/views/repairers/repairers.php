<table class="table table-bordered">
    <thead>
        <tr>
            <th>Texniki sahəsi</th>
            <th>Soyadı Adı</th>
            <th>Ünvan</th>
            <th>Tel</th>
        </tr>
    </thead>

    <?php foreach ($posts as $item) { ?>
        <tbody>
            <tr>
                <td><?= $item['profile']; ?></td>
                <td><?= $item['full_name']; ?></td>
                <td><?= $item['address']; ?></td>
                <td><?= $item['contact_num']; ?></td>
            </tr>
        </tbody>
    <?php } ?>
</table>