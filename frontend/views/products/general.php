<?php use yii\helpers\Url; ?>

<div class="wrap-category general-list-product">
    <div>
        <!--<div class="list-items-product">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Say</th>
                    <th>Maşının markası</th>
                    <th>Elanın miqdarı</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td><a href="#">Acura</a></td>
                    <td>23</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="#">Aprilia</a></td>
                    <td>5</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="#">Renault</a></td>
                    <td>6</td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td><a href="#">Audi</a></td>
                    <td>258</td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td><a href="#">BMW</a></td>
                    <td>14</td>
                </tr>
                <tr>
                    <th scope="row">6</th>
                    <td><a href="#">Mazda</a></td>
                    <td>54</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="#">Aprilia</a></td>
                    <td>5</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="#">Renault</a></td>
                    <td>6</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="#">Aprilia</a></td>
                    <td>5</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="#">Renault</a></td>
                    <td>6</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="#">Aprilia</a></td>
                    <td>5</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="#">Renault</a></td>
                    <td>6</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td><a href="#">Aprilia</a></td>
                    <td>5</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="#">Renault</a></td>
                    <td>6</td>
                </tr>
                </tbody>
            </table>
        </div>-->

        <div class="list-items-product">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Say</th>
                        <th>Maşının markası</th>
                        <th>Elanın miqdarı</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  foreach ($products as $id => $product) {?>
                        <tr>
                            <th scope="row"><?php echo $id + 1; ?></th>
                            <td>
                                <a href="<?php echo Url::home(); ?>products/modellist/<?php echo $product['car_brand']; ?>">
                                    <?php echo $product['car_brand_name']; ?>
                                </a>
                            </td>
                            <td><?php echo $product['car_brand_count']; ?></td>
                        </tr>
                    <?php }; ?>
                </tbody>
            </table>
        </div>
        <div class="clr"></div>
    </div>

    <nav class="pagination-wrap">
        <ul class="pagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>