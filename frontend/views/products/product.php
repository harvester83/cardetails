<div class="wrap-category repairers clearfix">
    <h2 class="block-title top-line block-title-product"><?php echo 'ID - '. $product['product_id'];?></h2>
    <div class="single-product clearfix">
        <div class="block-parameter">
            <div class="block">
                <h2 class="block-title">Göstəricilər</h2>
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th scope="row">Hissənin adı</th>
                            <td><?php echo $product['product_name']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Seriya nömrəsi</th>
                            <td><?php echo $product['serial_number']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Marka üçün</th>
                            <td><?php echo $product['car_brand_name']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Model üçün</th>
                            <td><?php echo $product['car_model_name']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">İstehsal ili</th>
                            <td><?php echo $product['production_year']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">İstehsalçı</th>
                            <td><?php echo $product['manufacturer']; ?></td>
                        </tr>
                        <tr>
                            <th scope="row">İstehsalçı ölkə</th>
                            <td><?php echo $product['production_country_name']; ?></td>
                        </tr>

                        <?php
                            if(isset($product['mileage']) && $product['mileage'] == true ){?>
                            <tr>
                                <th scope="row">Yürüş</th>
                                <td><?php echo $product['mileage']; ?> km</td>
                            </tr>
                        <?php }?>

                        <tr>
                            <th scope="row">Rəngi</th>
                            <td><?php echo $product['color_name']; ?></td>
                        </tr>

                        <?php
                             if($product['delivery'] == 1){?>
                                <tr>
                                    <th scope="row">Çatdırılma</th>
                                    <td>daxildir</td>
                                </tr>
                        <?php }?>

                        <?php
                            if($product['setup'] == 1){?>
                                <tr>
                                    <th scope="row">Quraşdırılma</th>
                                    <td>daxildir</td>
                                </tr>
                        <?php }?>

                        <?php
                        if($product['guarantee'] == 1){?>
                            <tr>
                                <th scope="row">Zəmanət</th>
                                <td>daxildir</td>
                            </tr>
                        <?php }

                        if($product['credit'] == 1){?>
                            <tr>
                                <th scope="row">Kredit</th>
                                <td> + </td>
                            </tr>
                        <?php }; ?>
                        <tr>
                            <th scope="row">Qiyməti</th>
                            <td><?php echo $product['price']; ?></td>
                        </tr>

                        <?php if($product['productCategoryTypeName']) { ?>

                            <tr>
                                <th scope="row">Kateqoriya</th>
                                <td><?php
                                        if ($product['productCategoryTypeName'] === '#interior') {
                                            echo 'Interyer';
                                        }

                                        if ($product['productCategoryTypeName'] === '#exterior') {
                                            echo 'Eksteryer';
                                        }

                                        if ($product['productCategoryTypeName'] === '#action-part') {
                                            echo 'Hərəkət hissəsi';
                                        }

                                        if ($product['productCategoryTypeName'] === '#general') {
                                            echo 'Ümumi hissələr';
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php }

                        if($product['barter'] == 1){?>
                            <tr>
                                <th scope="row">Barter</th>
                                <td> + </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>

            <div class="block">
                <h2 class="block-title">Əlavə məlumat</h2>

                <div class="block-content">
                    <p><?php echo $product['editor']; ?></p>
                </div>
            </div>

            <div class="block-content">
                <p class="block-parameter-item">Yeniləndi:<span><?php echo $product['date'];?></span></p>
                <p class="block-parameter-item">Elanın nömrəsi:<span><?php echo $product['product_id'];?></span></p>
            </div>

            <div class="block block-info block-content">
                <p class="block-parameter-item">Satıcının adı:<span class="color-red"><?php echo $product['seller_name']; ?></span></p>
                <p class="block-parameter-item">Tel.:<span><?php echo $product['seller_phone1']; ?></span></p>
                <p class="block-parameter-item">Tel.:<span><?php echo $product['seller_phone2']; ?></span></p>
            </div>

            <div class="fb-comments" data-href="https://www.facebook.com/Topcar.az/?fref=ts" data-numposts="5">

            </div>


            <button class="button-action">Elanı Şikayyət et</button>
            <div class="block-appeal">
                <form action="" method="POST">
                    <p><input name="email" type="text" placeholder="email"></p>
                    <p><textarea name="message" id="" cols="30" rows="10" placeholder="məzmun"></textarea></p>
                    <p><input name="send" class="button-standard" type="submit" value="Göndər"></p>
                </form>
            </div>
        </div>

        <div class="block-images">
            <?php
            if(isset($product['photos'])) {
                foreach($product['photos'] as $id => $photo) {?>
                    <a href="#"><img src="<?php echo Yii::$app->homeUrl.'frontend/web'.$photo['medium']; ?>" alt=""></a>
                <?php };
            }; ?>

        </div>
    </div>
</div>