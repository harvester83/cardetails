<?php use yii\helpers\Url; ?>

<div class="wrap-category clearfix">
    <h2 class="block-title">Hissələr</h2>
    <div class="category">
        <?php foreach ($products as $product) { ?>
            <div class="product">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Malın adı</th>
                            <th>Hissənin İD nömrəsi</th>
                            <th>Maşın markası</th>
                            <th>Maşın modeli</th>
                            <th>İli</th>
                            <th>Qiyməti</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $product['product_name']; ?></td>
                            <td><?php echo $product['serial_number']; ?></td>
                            <td><?php echo $product['car_brand_name']; ?></td>
                            <td><?php echo $product['car_model_name']; ?></td>
                            <td><?php echo $product['production_year']; ?></td>
                            <td><?php echo $product['price']; ?> AZN</td>
                        </tr>
                    </tbody>
                </table>
                <div class="product-description">
                    <div class="product-note">
                        <p><span>Qeyd: </span><?= $product['editor']?></p>
                        <a href="<?php echo Url::toRoute('products/product/' . $product['product_id']); ?>"
                           class="more">Ətraflı</a>
                    </div>
                    <?php if (!empty($product['photo_main'])) { ?>
                        <img src="/frontend/web/<?php echo $product['photo_main'] ?>" alt="">
                    <?php } ?>
                    <div class="clr"></div>
                </div>
            </div>
        <?php }; ?>
    </div>
</div>

<nav class="pagination-wrap" id="pagination-products">
    <ul class="pagination">
        <li class="pagination-prew">
            <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>

        <?php for($i = 1; $i <= $pagesCount; $i++){ ?>
            <li><a href="#"><?php echo $i; ?></a></li>
        <?php }; ?>

        <li class="pagination-next">
            <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>