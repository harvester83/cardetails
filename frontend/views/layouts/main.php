<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$product = new \backend\models\Product();
$carList = $product->getCarList();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script type="application/javascript">
            absoluteUrl = '<?php echo Url::base(); ?>';
            homeUrl = '<?php echo Url::home(); ?>';
            carList = JSON.parse('<?php echo json_encode($product->getCarList()); ?>');
        </script>
        <?php $this->head() ?>
    </head>
    <body>


    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <?php $this->beginBody() ?>

    <div class="wpapper">
        <header class="top-header">
            <div class="ad-layout">
                <div class="ad-block-item">
                    <a href=""><img src="<?php echo Url::home(); ?>img/img-01-320x215.jpg" alt="Sasas"></a>
                </div>

                <div class="ad-block-item">
                    <a href=""><img src="<?php echo Url::home(); ?>img/img-02-320x215.jpg" alt=""></a>
                </div>

                <div class="ad-block-item">
                    <a href=""><img src="<?php echo Url::home(); ?>img/img-03-320x215.jpg" alt=""></a>
                </div>
                <div class="clr"></div>
            </div>
        </header>



        <div class="container">
            <aside class="control-panel">
                <div class="main-header">
                    <div class="logo">
                        <h1>oluxana</h1>
                        <a href="<?php echo Url::home(); ?>"><img src="<?php echo Url::home(); ?>img/logo.png" alt=""></a>
                    </div>
                </div>

                <div class="block">
                    <div class="block-wrap">

                        <h2 class="block-title">Menu</h2>
                        <nav class="menu">
                            <ul>
                                <li>
                                    <a <?php if (Yii::$app->controller->id == 'products' && Yii::$app->controller->action->id == 'add') {
                                        echo 'class="active-item"';
                                    }; ?> href="<?php echo Url::home(); ?>products/add">Hissəni əlavə et</a>
                                </li>

                                <li>
                                     <a <?php if (Yii::$app->controller->id == 'products' && Yii::$app->controller->action->id !== 'add' && Yii::$app->controller->action->id !== 'general-products-list') {
                                         echo 'class="active-item"';
                                     }; ?>href="<?php echo Url::home(); ?>products">Hissələr</a>
                                </li>

                                <li>
                                    <a <?php if (Yii::$app->controller->id == 'declarations') {
                                        echo 'class="active-item"';
                                    }; ?>href="<?php echo Url::home(); ?>declarations">Elanlar</a>
                                </li>

                                <li>
                                    <a <?php if (Yii::$app->controller->id == 'repairers') {
                                        echo 'class="active-item"';
                                    }; ?>href="<?php echo Url::home(); ?>repairers">Ustaların siyahsı</a>
                                </li>

                                <li><a href="mailto:info@oluxana.az">Bizimlə əlaqə</a></li>

                                <li>
                                    <a <?php if (Yii::$app->controller->id == 'searches') {
                                        echo 'class="active-item"';
                                    }?>href="<?php echo Url::home(); ?>searches">Geniş Axtarış</a>
                                </li>

                                <li>
                                    <a <?php if (Yii::$app->controller->action->id == 'general-products-list') {
                                        echo 'class="active-item"';
                                    }?>href="<?php echo Url::home(); ?>products/general-products-list">Ümumi hissələr</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="block">
                    <div class="block-wrap">
                        <h2 class="block-title">Filtrasiya</h2>

                        <form action="#" method="POST" class="filtration">
                            <div class="select-wrap">
                                <select id="car-brand-filtration">
                                    <?php foreach ($carList as $carBrandId => $car){ ?>
                                        <option value="<?php echo $carBrandId; ?>"><?php echo $car['brand']; ?></option>
                                    <?php }; ?>
                                </select>
                            </div>

                            <div class="select-wrap">
                                <select id="car-model-filtration">
                                    <?php foreach ($carList[1]['model'] as $modelId => $modelName) { ?>
                                        <option value="<?php echo $modelId; ?>"><?php echo $modelName; ?></option>
                                    <?php }; ?>
                                </select>
                            </div>

                            <div class="select-wrap">
                                <div>
                                    <input type="radio" name="isNew" id="utilized" checked>
                                    <label for="utilized">İşlənmiş</label>
                                </div>

                                <div>
                                    <input type="radio" name="isNew" id="new">
                                    <label for="new">Təzə</label>
                                </div>
                            </div>

                            <div class="select-wrap">
                                <button class="filtration-submit">Axtar</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="fb-page"
                     data-href="https://www.facebook.com/Topcar.az/?fref=ts"
                     data-tabs="timeline"
                     data-small-header="false"
                     data-adapt-container-width="true"
                     data-hide-cover="false"
                     data-show-facepile="true">
                    <div class="fb-xfbml-parse-ignore">
                        <blockquote cite="https://www.facebook.com/Topcar.az/?fref=ts">
                            <a href="https://www.facebook.com/Topcar.az/?fref=ts">Topcar.az</a>
                        </blockquote>
                    </div>
                </div>

            </aside>

            <div class="content">
                <div class="search-block">
                    <input class="general-search-field" type="text">
                    <input id="general-search-button" type="submit" value="AXTAR">
                    <span>Tez axtarış üçün malın id nömrəsini əlavə edin</span>
                </div>
               <?= $content; ?>
            </div>
        </div>

        <!--Ad banner block -->
        <div class="adbaners">
            <div class="adbaners-wrap">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
                into
                electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                release of
                Letraset sheets containing Lorem

                <div class="ad-image-banner">
                    <a href=""><img src="<?php echo Url::home(); ?>img/img-03-260x130.jpg" alt=""></a>
                </div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    </div>

    <footer>
        <div class="wrap-footer">
            <div>
                <div class="copyright">
                    <p>Copyright &copy; <?= date('Y') ?> OluXana.Az</p>
                    <p>
                        Administrasiya saytda yerləşdirilən banner və reklam məzmununa görə məsuliyyət daşımır.
                        Hər hansı bir məlumatı, materialı və fotoşəkili administrasiyanın yazılı icazəsi olmadan istifadə
                        etmək
                        qeyri-qanuni hesab ediləcək və Azərbaycan Respublikasının Qanunlarına əsasən cəzalandırılacaqdır.
                    </p>
                </div>

                <div class="contact">
                    <div class="con-info clearfix">
                        <span>Tel: +994(50) 668 7549</span>
                        <a class="contact-button" href="mailto:Eu40d@mail.ru">info@oluxana.az</a>
                    </div>
                </div>
                <div class="clr"></div>
            </div>

        </div>
    </footer>

    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>