<?php foreach ($declarations as $declaration) { ?>
    <div class="block-declaration">
        <div class="block">
            <h3>Axtarılan hissənin adı: <?php echo $declaration['product_name']; ?></h3>
        </div>

        <div class="block">
            <div class="block-content">
                <p><?php echo $declaration['editor']; ?></p>
            </div>
        </div>

        <div class="block-param-declaration">
            <p><span>Yeniləndi: </span><?php echo $declaration['date']; ?></p>
            <p><span>Elanın nömrəsi: </span><?php echo $declaration['declaration_id']; ?></p>
            <p><span>Alıcının adı: </span><?php echo $declaration['buyer']; ?></p>
            <p><span>Tel.: </span><?php echo $declaration['phone']; ?></p>
        </div>
    </div>
<?php }; ?>