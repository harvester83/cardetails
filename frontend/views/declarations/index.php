<?php use yii\helpers\Url; ?>
<div class="wrap-category clearfix">
    <h2 class="block-title">Elanlar
        <a href="<?php echo Url::home(); ?>declarations/add" class="add-declaration-item" title="Elanınızı yerləşdirin">+</a>
    </h2>
    <div class="category">

        <?php foreach ($declarations as $declaration) { ?>
            <div class="block-declaration">
                <div class="block">
                    <h3>Axtarılan hissənin adı: <?php echo $declaration['product_name']; ?></h3>
                </div>

                <div class="block">
                    <div class="block-content">
                        <p><?php echo $declaration['editor']; ?></p>
                    </div>
                </div>

                <div class="block-param-declaration">
                    <p><span>Yeniləndi: </span><?php echo $declaration['date']; ?></p>
                    <p><span>Elanın nömrəsi: </span><?php echo $declaration['declaration_id']; ?></p>
                    <p><span>Alıcının adı: </span><?php echo $declaration['buyer']; ?></p>
                    <p><span>Tel.: </span><?php echo $declaration['phone']; ?></p>
                </div>
            </div>
        <?php }; ?>
    </div>

    <nav class="pagination-wrap" id="pagination-declaration">
        <ul class="pagination">
            <li class="pagination-prew">
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>

            <?php for ($i = 1; $i <= $pagesCount; $i++) { ?>
                <li><a href="#"><?php echo $i; ?></a></li>
            <?php }; ?>

            <li class="pagination-next">
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>

