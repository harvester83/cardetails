<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Oluxana';
?>
<div class="wrap-category">
    <h2 class="block-title">Son əlavə edilmiş elanlar</h2>
    <div class="category">
        <ul class="products-list">
            <?php foreach ($dataProduct as $productKey => $productValue) {?>
                <li class="product-featured">
                    <h3>
                        <a href="<?php echo Url::toRoute('products/product/' . $productValue['product_id']); ?>"><?php echo $productValue['product_name']; ?></a>
                    </h3>
                    <h4><?php echo $productValue['car_brand_name']; ?> | <?php echo $productValue['car_model_name']; ?></h4>
                        <a href="<?php echo Url::toRoute('products/product/' . $productValue['product_id']); ?>">
                            <?php $thumbsImg = unserialize($productValue['thumbs']); ?>
                            <img src="<?php echo Yii::$app->homeUrl.'frontend/web'.$thumbsImg['small']; ?>" alt="#">

                        </a>
                    <span><?php echo $productValue['price']; ?> AZN</span>
                </li>
            <?php }; ?>
            <div class="clr"></div>
        </ul>
    </div>
</div>
<div class="next-ad-posts"><a href="<?php echo Url::home(); ?>products/">Növbəti elanlara bax</a></div>

<div class="list-items-product-wrap">
    <div>
        <div class="list-items-product">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Say</th>
                        <th>Maşının markası</th>
                        <th>Elanın miqdarı</th>
                    </tr>
                </thead>
                <tbody>
                <?php  foreach ($dataProductBrand as $id => $product) {?>
                    <tr>
                        <th scope="row"><?php echo $id + 1; ?></th>
                        <td>
                            <a href="<?php echo Url::home(); ?>products/getmodellist/<?php echo $product['car_brand']; ?>">
                                <?php echo $product['car_brand_name']; ?>
                            </a>
                        </td>
                        <td><?php echo $product['car_brand_count']; ?></td>
                    </tr>
                <?php }; ?>
                </tbody>
            </table>
        </div>

        <div class="clr"></div>
    </div>
</div>

