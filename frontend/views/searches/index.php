<div class="wrap-category">
    <h2 class="block-title top-line">Geniş axtarış</h2>

    <div class="add-profuct-cat">
        <div class="form-content clearfix">
            <form id="signupForm" action="#" method="POST">
                    <div class="ol-col-1">
                        <div>
                            <label>Marka üçün</label>
                            <select id="car-brand">
                                <?php foreach ($carList as $carBrandId => $car){ ?>
                                    <option value="<?php echo $carBrandId; ?>"><?php echo $car['brand']; ?></option>
                                <?php }; ?>
                            </select>
                        </div>

                        <div>
                            <label>Model üçün</label>
                            <select id="car-model">
                                <?php foreach ($carList[1]['model'] as $modelId => $model) { ?>
                                    <option value="<?php echo $modelId; ?>"><?php echo $model; ?></option>
                                <?php }; ?>
                            </select>
                        </div>

                        <div>
                            <label>Istehsal ili</label>
                            <select id="production-year"></select>
                        </div>

                        <div>
                            <label>Hansı il üçün min</label>
                            <select id="for-year-min"></select>
                        </div>

                        <div>
                            <label>Hansı il üçün max</label>
                            <select id="for-year-max"></select>
                        </div>

                        <div>
                            <label>Istehsalçı ölkə</label>
                            <select id="production-country">
                                <?php foreach ($countries as $countryKey => $countryVal) { ?>
                                    <option value="<?php echo $countryKey?>"><?php echo $countryVal?></option>
                                <?php }; ?>
                            </select>
                        </div>

                        <div>
                            <label>Rəngi</label>
                            <select id="color">
                                <?php foreach ($colors as $colorsKey => $colorsVal) {?>
                                    <option value="<?php echo $colorsKey?>"><?php echo $colorsVal?></option>
                                <?php }; ?>
                            </select>
                        </div>

                        <div>
                            <label>Yürüş</label>
                            <input class="input-text" id="mileage" title="Yurush" type="number" step="1000" value="0">
                        </div>

                        <div>
                            <label>Qiyməti</label>
                            <input class="input-text" id="price" title="Qiyməti" type="text" value="0">
                        </div>

                        <div>
                            <label>Şəhər</label>
                            <select id="city">
                                <?php foreach ($cities as $citiesKey => $citiesVal) {?>
                                    <option value="<?php echo $citiesKey?>"><?php echo $citiesVal?></option>
                                <?php }; ?>
                            </select>
                        </div>
                    </div>

                    <div class="ol-col-2">
                        <div class="input-group-admin">
                            <div><input id="utilized-search" name="check-button" type="radio"><label for="utilized">İşlənmiş</label></div>
                            <div><input id="new-search" name="check-button" type="radio" checked><label for="new">Təzə</label></div>
                        </div>

                        <div class="input-group-admin">
                            <div><input id="credit" name="credit" type="checkbox"><label for="credit">Kredit</label></div>
                            <div><input id="guarantee" type="checkbox"><label for="guarantee">Zəmanət</label></div>
                            <div><input id="barter" type="checkbox"><label for="barter">Barter</label></div>
                            <div><input id="setup" type="checkbox"><label for="setup">Quraşdırma daxildir</label></div>
                            <div><input id="delivery" type="checkbox"><label for="delivery">Catdirilma</label></div>



                            <div class="checkbox-group text">Vəziyyət</div>
                            <div>
                                <input id="perfect" name="check-button-quality" type="radio" checked>
                                <label for="perfect">Əla</label>
                            </div>
                            <div>
                                <input id="average" name="check-button-quality" type="radio">
                                <label for="average">Orta</label>
                            </div>
                            <div>
                                <input id="notwork" name="check-button-quality" type="radio">
                                <label for="notwork">İşləmir</label>
                            </div>

                            <div class="checkbox-group text">Kategoriya</div>
                            <div>
                                <input id="interior" name="check-button-category" type="radio">
                                <label for="interior">Interyer</label>
                            </div>
                            <div>
                                <input id="exterior" name="check-button-category" type="radio">
                                <label for="exterior">Eksteryer</label>
                            </div>
                            <div>
                                <input id="action-part" name="check-button-category" type="radio">
                                <label for="action-part">Hərəkət hissəsi</label>
                            </div>
                            <div>
                                <input id="general" name="check-button-category" type="radio" checked>
                                <label for="general">Ümumi hissələr</label>
                            </div>
                        </div>
                    </div>

                <button type="submit" class="all-search button-standard">Axtar</button>
            </form>
    </div>
</div>
