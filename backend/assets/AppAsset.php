<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
         'css/common.css',
         'css/product.css',
         'css/repairer.css',
         'css/bootstrap-datetimepicker.css',
         'css/bootstrap-combined.no-icons.min.css',
         'css/bootstrap-responsive.min.css',
         'css/font-awesome.css',
         'css/wysiwyg.css',
         'css/declarations.css',
         'css/qunit-1.13.0.css',
    ];
    public $js = [
        'js/jquery.hotkeys.js',
        'js/jquery.screwdefaultbuttonsV2.min.js',
        'js/jquery.validate.js',
        'js/content.js',
        'js/products.js',
        'js/moment-with-locales.js',
        'js/repairers.js',
        'js/category.js',
        'js/prettify.js',
        'js/initToolbarBootstrapBindings.js',
        'js/datetimepicker.js',
        'js/notifyjs-v0.3.1.js',
        'js/bootstrap-wysiwyg.js',
        'js/main.js',
        'js/file-choser.js',
        'js/declarations.js',
        'js/jquery.twbsPagination.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
