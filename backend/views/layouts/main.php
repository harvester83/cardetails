<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);

$product = new \backend\models\Product();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script type="application/javascript">
        absoluteUrl = '<?php echo Url::base(); ?>';
        homeUrl = '<?php echo Url::base(); ?>';
        carList = JSON.parse('<?php echo json_encode($product->getCarList()); ?>');
        limitLink = '<?php echo json_encode($product->getLinkLimit()); ?>';
    </script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="site-index">
    <?php
        NavBar::begin([
            'brandLabel' => 'Oluxana',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        $menuItems = [
            ['label' => 'Home', 'url' => ['/site/index']],
        ];
        if (Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        } else {
            $menuItems[] = [
                'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
            ];
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li <?php if (Yii::$app->controller->id == 'products') {
                        echo 'class="active"';
                    }; ?>><a href="<?php echo Url::home(); ?>/products/index">Hissələr</a>
                    </li>

                    <li <?php if (Yii::$app->controller->id == 'declarations') {
                        echo 'class="active"';
                        }; ?>><a href="<?php echo Url::home(); ?>/declarations/index">Elanlar</a>
                    </li>

                    <li <?php if (Yii::$app->controller->id == 'repairers') {
                        echo 'class="active"';
                    }; ?>><a href="<?php echo Url::home(); ?>/repairers/index">Ustalar</a>
                    </li>

                    <li <?php if (Yii::$app->controller->module->id == 'filemanager') {
                        echo 'class="active"';
                    }; ?>><a href="<?php echo Url::home(); ?>/filemanager/">Media</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
