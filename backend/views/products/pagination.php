<ul class="pagination" id="pagination-posts" data-link="<?php echo $linkLimitIndex; ?>">
    <span>
        <a class="previous" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
        </a>
    </span>

    <?php foreach ($linkLimit[$linkLimitIndex] as $link):?>
        <li><a href="#"><?php echo $link; ?></a></li>
    <?php endforeach; ?>

    <span>
        <a class="next" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </span>
</ul>