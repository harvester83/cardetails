<div class="table-responsive">
    <div class="products-list">
        <div>
            <h2 class="sub-header">Hissələr</h2>
            <div class="product-search-form">
                <input class="product-search-form-text" type="text" placeholder="Axtar">
                <input class="product-search-form-submit" type="submit" value="AXTAR">
                <p>Elanın İD nömrəsinə görə axtar</p>
            </div>
        </div>

        <div class="table-wrap">

            <table id="product-table" cellspacing="0" class="table table-striped">
                <thead class="products-list">
                    <tr>
                        <td>Elanın ID nömrəsi</td>
                        <td>Moderasiya</td>
                        <td style="width: 150px;">Malın adı</td>
                        <td>İstehsalçı</td>
                        <td>Seriya nömrəsi</td>
                        <td>Maşın markası</td>
                        <td>Maşın modeli</td>
                        <td>İstehsal ili</td>
                        <td>Elanı Yerləşdirmə Tarixi</td>
                        <td>Rəng</td>
                        <td>Qiymət</td>
                    </tr>
                </thead>

                <tbody id="products-item">
                    <?php foreach($products as $product) {?>
                        <tr class="post product" data-product-id="<?php echo $product['product_id']; ?>">
                            <td><?php echo $product['product_id']; ?></td>
                            <td>
                                <?php if ($product['moderation']) { ?>
                                    <span class="glyphicon glyphicon-ok"></span>
                                <?php } else { ?>
                                    <span class="glyphicon glyphicon-minus-sign"></span>
                                <?php };?>
                            </td>
                            <td><?php echo $product['product_name']; ?></td>
                            <td><?php echo $product['production_country_name']; ?></td>
                            <td><?php echo $product['serial_number']; ?></td>
                            <td><?php echo $product['car_brand_name']; ?></td>
                            <td><?php echo $product['car_model_name']; ?></td>
                            <td><?php echo $product['production_year']; ?></td>
                            <td><?php echo $product['date']; ?></td>
                            <td><?php echo $product['color_name']; ?></td>
                            <td><?php echo $product['price']; ?> AZN</td>
                            <td>
                                <button  type="button" class="preview-edit-product btn btn-warning btn-xs">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </button>
                            </td>
                            <td>
                                <button  type="button" class="delete-product btn btn-danger btn-xs">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>

        <p>
            <button id="add-product" type="submit" class="ol-btn-primary">Hissəni əlavə et</button>
        </p>

        <nav class="pagintaion-wrapper">
            <ul class="pagination" id="pagination-posts" data-link="<?php echo $linkLimitIndex; ?>">
                <span>
                    <a class="previous" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </span>
                <?php foreach ($linkLimit[$linkLimitIndex] as $link):?>
                    <li><a href="#"><?php echo $link; ?></a></li>
                <?php endforeach; ?>

                <span>
                    <a class="next" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </span>
            </ul>
        </nav>

    </div>

    <div id="page" class="add-product-field">
        <h2 class="sub-header">Hissə</h2>
        <form action="#" method="POST">
            <div>
                <div class="add-product-inputs">
                    <div>
                        <span class="add-product-input-span">Hissənin adı</span>
                        <input class="form-control" type="text" name="productName" placeholder="Malın adı" id="product-name">
                    </div>

                    <div>
                        <span class="add-product-input-span">İstehsalçı</span>
                        <input class="form-control" type="text" name="manufacturer" placeholder="İstehsalçı" id="manufacturer">
                    </div>
                    <div>
                        <span class="add-product-input-span">Seriya nömrəsi</span>
                        <input class="form-control" type="text" name="serialNumber" placeholder="Seriya nömrəsi" id="serial-number">
                    </div>

                    <div>
                        <label for="car-brand" class="add-product-input-span">Marka üçün</label>
                        <select id="car-brand">
                            <?php foreach ($carList as $carBrandId => $car){ ?>
                                <option value="<?php echo $carBrandId; ?>"><?php echo $car['brand']; ?></option>
                            <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <label for="car-model" class="add-product-input-span">Model üçün</label>
                        <select id="car-model">
                            <?php foreach ($carList[1]['model'] as $modelId => $model) { ?>
                                <option value="<?php echo $modelId; ?>"><?php echo $model; ?></option>
                            <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <label for="production-year" class="add-product-input-span">Istehsal ili</label>
                        <select id="production-year">
                            <?php for ($i = 2015; $i >= 1955; $i--) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <label for="for-year-min" class="add-product-input-span">Hansı il üçün min</label>
                        <select id="for-year-min">
                            <?php for ($i = 2015; $i >= 1955; $i--) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <label for="for-year-max" class="add-product-input-span">Hansı il üçün max</label>
                        <select id="for-year-max">
                            <?php for ($i = 2015; $i >= 1955; $i--){ ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php }; ?>
                        </select>
                    </div>
                </div>

                <div class="add-product-inputs">
                    <div>
                        <label for="production-country" class="add-product-input-span">Istehsalçı ölkə</label>
                        <select id="production-country">
                            <?php foreach ($countries as $countryKey => $country){ ?>
                                <option value="<?php echo $countryKey; ?>"><?php echo $country; ?></option>
                           <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <span class="add-product-input-span">Yürüş</span>
                        <input step="10000" max="" min="0" type="number" class="form-control" name="mileage" placeholder="Yürüş(km)" id="mileage">
                    </div>

                    <div>
                        <label for="color" class="add-product-input-span">Rəngi</label>
                        <select id="color">
                            <?php foreach ($colors as $colorKey => $color){ ?>
                                <option value="<?php echo $colorKey; ?>"><?php echo $color; ?></option>
                            <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <span class="add-product-input-span">Qiyməti</span>
                        <input class="form-control" type="number" name="price" placeholder="Qiyməti (AZN)" id="price">
                    </div>

                    <div>
                        <label for="city" class="add-product-input-span">Şəhər</label>
                        <select id="city">
                            <?php foreach ($cities as $cityKey => $city){ ?>
                                <option value="<?php echo $cityKey; ?>"><?php echo $city; ?></option>
                            <?php }; ?>
                        </select>
                    </div>

                    <div>
                        <span class="add-product-input-span">Satıcının adı</span>
                        <input class="form-control" type="text" name="sellerName" placeholder="Satıcının adı" id="seller-name">
                    </div>

                    <div>
                        <span class="add-product-input-span">Satıcının email ünvanı</span>
                        <input class="form-control" type="text" name="sellerMail" placeholder="Satıcının email ünvanı" id="seller-mail">
                    </div>

                    <div class="clearfix">
                        <span class="add-product-input-span">Satıcının tel nömrələri</span>
                        <div>
                            <input class="form-control" type="text" name="sellerPhone1" placeholder="tel" id="seller-phone1">
                            <input class="form-control" type="text" name="sellerPhone2" placeholder="tel" id="seller-phone2">
                        </div>
                        <div class="clr"></div>
                    </div>
                </div>
                <div class="clr"></div>
            </div>

            <div>
                <div class="input-group-admin">
                    <p><input id="credit" name="credit" type="checkbox" ><label for="credit">Kredit</label></p>
                    <p><input id="guarantee" name="guarantee" type="checkbox" ><label for="guarantee">Zəmanət</label></p>
                    <p><input id="barter" name="barter" type="checkbox" ><label for="barter">Barter</label></p>
                    <p><input id="setup" name="setup" type="checkbox" ><label for="setup">Quraşdırma daxildir</label></p>
                    <p><input id="delivery" name="delivery" type="checkbox"><label for="delivery">Catdirilma</label></p>
                </div>

                <div class="input-group-admin">
                    <p class="quality text">Vəziyyət</p>
                    <p><input id="perfect" name="quality" type="radio" checked ><label for="perfect">Əla</label></p>
                    <p><input id="average" name="quality" type="radio" ><label for="average">Orta</label></p>
                    <p><input id="notwork" name="quality" type="radio" ><label for="notwork">İşləmir</label></p>
                </div>

                <div id="input-group-admin" class="input-group-admin">
                    <div class="quality text">Kateqoriya</div>
                    <div>
                        <input id="general" class="category-button" name="category-part" checked value="4" type="radio"/>
                        <label for="general">Ümumi hissələr</label>
                    </div>
                    <div>
                        <input id="interior" class="category-button" name="category-part" value="1" type="radio"/>
                        <label for="interior">Interyer</label>
                    </div>
                    <div>
                        <input id="exterior" class="category-button" name="category-part" value="2" type="radio"/>
                        <label for="exterior">Eksteryer</label>
                    </div>

                    <div>
                        <input id="action-part" class="category-button" name="category-part" value="3" type="radio"/>
                        <label for="action-part">Hərəkət hissəsi</label>
                    </div>

                </div>

                <div class="input-group-admin">
                    <p><input id="new" name="quality2" type="radio"/><label for="new">Təzə</label></p>
                    <p><input id="utilized" name="quality2" type="radio"/><label for="utilized">İşlənmiş</label></p>
                </div>
                <p><input id="moderation" name="moderation" type="checkbox"/><label for="moderation">Moderasiya</label></p>
                <div class="clr"></div>
            </div>

            <!-- bootstrap-wysiwyg-->
            <div class="hero-unit">
                <div id="alerts"></div>
                <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="icon-font"></i><b class="caret"></b></a>
                        <ul class="dropdown-menu"></ul>
                    </div>
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                            <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                            <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                        <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
                        <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
                        <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
                        <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                        <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                        <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                        <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                        <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                    </div>
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>

                        <div class="dropdown-menu input-append">
                            <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                            <button class="btn" type="button">Add</button>
                        </div>
                        <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>

                    </div>

                    <div class="btn-group">
                        <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage"/>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                    </div>
                    <input type="hidden" name="text" data-edit="inserttext" id="text" x-webkit-speech="">
                </div>

                <div id="editor">

                </div>

            </div><!-- bootstrap-wysiwyg-->


            <div class="product-download-images clearfix">

                <div class="form-group">
                    <div class="input-group date" id="datetimepicker-photo-list">
                        <input type='text' class="form-control" name="date" id="selected-photos">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
            </div>

            <div id="photos">
              <?php if (!empty($photos)) foreach($photos as $key => $photo) { ?>
                  <div class="photo" data-photo-id="<?php echo $photo['id']; ?>">
                      <img  src="<?php echo Yii::$app->urlManagerFrontEnd->baseUrl . $photo['thumbs']['small']; ?>" alt="">
                      <span class="set-main">*</span>
                      <span class="delete">x</span>
                  </div>
              <?php }; ?>
                <div class="clr"></div>
            </div>
            <button class="show-images ol-btn-primary">Show ALL images</button>

            <div class="col-sm-3">
                <div class="form-group">
                    Hissənin dərc olunma tarixi
                    <div class="input-group date" id="datetimepicker2">
                        <input type="text" class="form-control"  name="date" id="date">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                    </div>
                </div>
                <button id="save-product" type="submit" class="ol-btn-primary">Save page</button>
                <button id="update-product" type="submit" class="ol-btn-primary">Update page</button>
            </div>
        </form>
    </div>
</div>





