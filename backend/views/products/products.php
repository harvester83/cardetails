<?php
foreach($products as $product) {?>
    <tr class="post product" data-product-id="<?php echo $product['product_id']; ?>">
        <td><?php echo $product['product_id']; ?></td>
        <td>
            <?php if ($product['moderation']) { ?>
                <span class="glyphicon glyphicon-ok"></span>
            <?php } else { ?>
                <span class="glyphicon glyphicon-minus-sign"></span>
            <?php };?>
        </td>
        <td><?php echo $product['product_name']; ?></td>
        <td><?php echo $product['production_country_name']; ?></td>
        <td><?php echo $product['serial_number']; ?></td>
        <td><?php echo $product['car_brand_name']; ?></td>
        <td><?php echo $product['car_model_name']; ?></td>
        <td><?php echo $product['production_year']; ?></td>
        <td><?php echo $product['date']; ?></td>
        <td><?php echo $product['color_name']; ?></td>
        <td><?php echo $product['price']; ?> AZN</td>
        <td>
            <button  type="button" class="preview-edit-product btn btn-warning btn-xs">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
        </td>
        <td>
            <button  type="button" class="delete-product btn btn-danger btn-xs">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </td>
    </tr>
<?php }?>

