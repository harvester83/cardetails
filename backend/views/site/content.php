<div class="col-sm-8 col-sm-offset-3 col-md-8 col-md-offset-2 main">
    <h2 class="sub-header">Edit Post</h2>
    <div class="table-responsive">
        <div id="page">
            <form action="<?php echo UrlHelper::getAbsoluteAdminUrl(); ?>pages/add" method="POST">

                <p><input class="form-control" value="<?php echo $_POST['title']; ?>" type="text" placeholder="Title" id="title" name="title"></p>
                <p><textarea class="form-control"  rows="15" placeholder="Content" name="text"><?php /*echo $_POST['text']; */?></textarea></p>
                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control" value="<?php /*echo $_POST['created']; */?>" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-3">
    <ul>
        <li><label><input type="checkbox"/>Pressa</label></li>
        <li><label><input type="checkbox"/>Multimedia</label></li>
        <li><label><input type="checkbox"/>Yaradıcılığı</label></li>
    </ul>
</div>