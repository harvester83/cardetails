<thead>
    <tr>
        <td>ID</td>
        <td>Moderasiya</td>
        <td>Hissənin adı</td>
        <td>Alıcının adı</td>
        <td>Tel</td>
        <td>Yerləşdirmə tarixi</td>
        <td>Redaktə et</td>
        <td>Sil</td>
    </tr>
</thead>

<tbody id="declarations-item">
    <tr data-declaration-id="<?php echo $declarationData['declaration']['declaration_id']; ?>" class="declaration">
        <td><?php echo $declarationData['declaration']['declaration_id']; ?></td>
        <td>
            <?php if ($declarationData['declaration']['moderation']) { ?>
                <span class="glyphicon glyphicon-ok"></span>
            <?php } else { ?>
                <span class="glyphicon glyphicon-minus-sign"></span>
            <?php };?>
        </td>
        <td><?php echo $declarationData['declaration']['product_name']; ?></td>
        <td><?php echo $declarationData['declaration']['buyer']; ?></td>
        <td><?php echo $declarationData['declaration']['phone']; ?></td>
        <td><?php echo $declarationData['declaration']['date']; ?></td>
        <td>
            <button  type="button" class="preview-edit-declaration btn btn-warning btn-xs">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
        </td>
        <td>
            <button type="button" class="delete-declaration btn btn-danger btn-xs">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </td>
    </tr>
</tbody>
