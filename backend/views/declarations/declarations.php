<?php foreach ($declarations as $declaration) { ?>
    <tr data-declaration-id="<?php echo $declaration['declaration_id']; ?>" class="declaration">
        <td><?php echo $declaration['declaration_id']; ?></td>
        <td>
            <?php if ($declaration['moderation']) { ?>
                <span class="glyphicon glyphicon-ok"></span>
            <?php } else { ?>
                <span class="glyphicon glyphicon-minus-sign"></span>
            <?php };?>
        </td>
        <td><?php echo $declaration['product_name']; ?></td>
        <td><?php echo $declaration['buyer']; ?></td>
        <td><?php echo $declaration['phone']; ?></td>
        <td><?php echo $declaration['date']; ?></td>
        <td>
            <button  type="button" class="preview-edit-declaration btn btn-warning btn-xs">
                <span class="glyphicon glyphicon-pencil"></span>
            </button>
        </td>
        <td>
            <button type="button" class="delete-declaration btn btn-danger btn-xs">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </td>
    </tr>
<?php }; ?>