<div class="table-responsive">
    <div class="declarations-list">
        <div>
            <h2>Elanlar</h2>
            <div class="declaration-search-form">
                <input class="declaration-search-form-text" type="text" placeholder="Axtar">
                <input class="declaration-search-form-submit" type="submit" value="AXTAR">
                <p>Elanın İD nömrəsinə görə axtar</p>
            </div>
        </div>

        <table id="declaration-table" cellspacing="0" class="table table-striped">
            <thead>
                <tr>
                    <td>ID</td>
                    <td>Moderasiya</td>
                    <td>Hissənin adı</td>
                    <td>Alıcının adı</td>
                    <td>Tel</td>
                    <td>Yerləşdirmə tarixi</td>
                    <td>Redaktə et</td>
                    <td>Sil</td>
                </tr>
            </thead>

            <tbody id="declarations-item">
            <?php foreach ($declarations as $declaration) { ?>
                <tr data-declaration-id="<?php echo $declaration['declaration_id']; ?>" class="declaration">
                    <td><?php echo $declaration['declaration_id']; ?></td>

                    <td>
                        <?php if ($declaration['moderation']) { ?>
                            <span class="glyphicon glyphicon-ok"></span>
                        <?php } else { ?>
                            <span class="glyphicon glyphicon-minus-sign"></span>
                        <?php };?>
                    </td>

                    <td><?php echo $declaration['product_name']; ?></td>
                    <td><?php echo $declaration['buyer']; ?></td>
                    <td><?php echo $declaration['phone']; ?></td>
                    <td><?php echo $declaration['date']; ?></td>
                    <td>
                        <button  type="button" class="preview-edit-declaration btn btn-warning btn-xs">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                    </td>
                    <td>
                        <button type="button" class="delete-declaration btn btn-danger btn-xs">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                </tr>
            <?php }; ?>

            </tbody>
        </table>
        <div><button id="add-declaration" class="ol-btn" type="submit">ƏLAVƏ ET</button></div>

        <nav class="pagination-wrap" id="pagination-declaration">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>

                <?php for($i =1; $i <= $pagesCount; $i++){ ?>
                    <li><a href="#"><?php echo $i; ?></a></li>
                <?php }; ?>

                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>

    <div id="page" class="add-declaration-field wrap-declarations-form">
        <h2 class="sub-header">Elanı əlavə et</h2>
        <form id="add-declaration-form" action="#" method="POST">
            <div>
                <label for="">Hissənin adı</label>
                <input class="form-control" type="text" placeholder="Hissənin adı" id="product-name" name="productName" required>
            </div>

            <!-- bootstrap-wysiwyg-->
            <div class="hero-unit">
                <div id="alerts"></div>
                <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="icon-font"></i><b class="caret"></b></a>
                        <ul class="dropdown-menu"></ul>
                    </div>
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                            <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                            <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                        </ul>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                        <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
                        <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
                        <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
                        <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                        <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                        <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                        <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                        <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                    </div>
                    <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>

                        <div class="dropdown-menu input-append">
                            <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                            <button class="btn" type="button">Add</button>
                        </div>
                        <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>

                    </div>

                    <div class="btn-group">
                        <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage"/>
                    </div>
                    <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                    </div>
                    <input type="hidden" name="text" data-edit="inserttext" id="text" x-webkit-speech="">
                </div>

                <div id="editor"></div>
            </div><!-- bootstrap-wysiwyg-->

            <div>
                <label for="">Adı</label>
                <input class="form-control" type="text" placeholder="Adı" id="buyer" name="buyer" required>
            </div>

            <div>
                <label for="">Email</label>
                <input class="form-control" type="text" placeholder="email" id="email" name="email" required>
            </div>

            <div>
                <label for="">Tel.:</label>
                <input class="form-control" type="text" placeholder="Tel.:" id="phone" name="phone" required>
            </div>

            <div>
                <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" name="date" id="date">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                 <div class="declaration-moderation-label">
                     <input id="moderation" name="moderation" type="checkbox"/>
                     <label for="moderation">Moderasiya</label>
                 </div>

                <button id="save-declaration" type="submit" class="ol-btn btn btn-primary">YADDA SAXLA</button>
                <button id="update-declaration"  type="submit" class="ol-btn btn btn-primary">YENILƏ</button>
            </div>
        </form>
    </div>
</div>





