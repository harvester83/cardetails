<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\db\mssql;
use yii\db\Query;

class Repairer extends Model
{
    const TABLE_NAME = 'repairers';
    private $limit = 5;

    /**
     * @param int $pageNum
     * @return array
     */
    public function getRepairers($pageNum)
    {
        $start = !empty($pageNum) ? ($pageNum - 1) * $this->limit : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
             SELECT *
             FROM `repairers`
             ORDER BY `date` DESC
             LIMIT :start, :limit
          "
        );

        $command->bindParam(':start', $start);
        $command->bindParam(':limit', $this->limit);
        $dataReader = $command->query();
        $dataReader = $dataReader->readAll();

        return $dataReader;
    }

    /**
     * @param $postId
     * @return array
     */
    public function getRepairer($postId)
    {
        $query = new Query;
        $columns = ['repairer_id', 'profile', 'full_name', 'address', 'contact_num', 'date'];

        $query->select($columns)
            ->from('repairers')
            ->where('repairer_id = :id', array(':id' => $postId))
            ->limit(1);

        $command = $query->createCommand();

        return $command->queryOne();
    }

    /**
     * @param int $postId
     * @param string  $profile
     * @param string $fullName
     * @param string $address
     * @param string $contactNum
     * @param int $date
     * @return bool
     * @throws \yii\db\Exception
     */
    public function editRepairer($postId, $profile, $fullName, $address, $contactNum, $date)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            UPDATE `repairers`
            SET `profile`     = :profile,
                `full_name`   = :fullName,
                `address`     = :address,
                `contact_num` = :contactNum,
                `date`        = :date
            WHERE `repairer_id`   = :id
        ');

        $command->bindValues(array(
                ':profile'    => $profile,
                ':fullName'   => $fullName,
                ':address'    => $address,
                ':contactNum' => $contactNum,
                ':date'       => $date,
                ':id'         => $postId,
            )
        );

        $command->execute();

        return true;
    }

    /**
     * @param string $profile
     * @param string $fullName
     * @param string $address
     * @param string $contactNum
     * @param int $date
     * @return int
     * @throws \yii\db\Exception
     */
    public function addRepairer($profile, $fullName, $address, $contactNum, $date)
    {
        // add ramdom reapairers (posts) (емуляция данных в базу)
       for ($i = 1; $i <= 10; $i++) {
            $connection = Yii::$app->db;
            $command = $connection->createCommand('
             INSERT `repairers`
              SET `profile`     = :profile,
                  `full_name`   = :fullName,
                  `address`     = :address,
                  `contact_num` = :contactNum,
                  `date`        = :date
            ');

            $testProfile = array('Elektrik', 'Rəngsaz', 'Buferşik', 'Моторист', 'Сварщик', 'Карбюраторщик');
            $randTestProfile = array_rand($testProfile, 1);

            $testFullName = array('Polad', 'İsimxan', 'Əlövsət', 'Yalçın', 'Eldəniz', 'Rinat');
            $randTestFullName = array_rand($testFullName, 1);

            $testAddress = array('г. Баку, Шарифзаде 196', 'г. Баку, Бакиханова 7', 'г. Баку, Джафара Джаббарлы 44', 'г. Баку, 28 Мая 33', 'г. Баку, Бюль-Бюля пр-т. 4', 'г. Баку, Гамбарова Рамиза 11');
            $randTestAddress = array_rand($testAddress, 1);

            $testContactNum = array('070-976-12-54', '070-886-56-12', '055-376-73-12', '050-776-39-17', '070-456-36-12', '070-976-12-54');
            $randTestContactNum = array_rand($testContactNum, 1);

            $testDate = array('2015-10-16', '2015-12-19', '2015-11-17', '2015-07-16', '2015-08-06', '2015-11-30');
            $randTestDate = array_rand($testDate, 1);

            $command->bindValues(array(
                    ':profile'     => $testProfile[$randTestProfile],
                    ':fullName'    => $testFullName[$randTestFullName],
                    ':address'     => $testAddress[$randTestAddress],
                    ':contactNum'  => $testContactNum[$randTestContactNum],
                    ':date'        => $testDate[$randTestDate]
                )
            );

            $command->execute();
        }

       /* $connection = Yii::$app->db;
        $command = $connection->createCommand('
             INSERT `repairers`
              SET `profile`     = :profile,
                  `full_name`   = :fullName,
                  `address`     = :address,
                  `contact_num` = :contactNum,
                  `date`        = :date
        ');

        $command->bindValues(array(
            ':profile'     => $profile,
            ':fullName'    => $fullName,
            ':address'     => $address,
            ':contactNum'  => $contactNum,
            ':date'        => $date
            )
        );

        $command->execute();*/

        $lastId = Yii::$app->db->getLastInsertID();

        return $lastId;
    }

    /**
     * @param int $postid
     * @return bool
     * @throws \yii\db\Exception
     */
    public function deleteRepairer($postid)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            DELETE FROM `repairers`
            WHERE `repairer_id`=:id
        ');

        $command->bindParam(':id', $postid);
        $command->execute();

        return true;
    }

    /**
     * @return int float
     */
    public function getPagesCount()
    {
        $connection = Yii::$app->db;
        $totalCountRows = $connection->createCommand('
            SELECT COUNT(*)
            FROM `repairers`
        ');

        $totalCountRows = $totalCountRows->queryScalar();
        $numPagination = ceil($totalCountRows / $this->limit);

        return $numPagination;
    }

    /**
     * @param int $id
     * @return array|bool
     */
    public function getSearchID($id)
    {
        $connection = Yii::$app->db;

        $command = $connection->createCommand('
           SELECT *
           FROM  `repairers`
           WHERE `repairer_id` = :id
        ');

        $command->bindValue(':id', $id);
        $repairerData = $command->queryOne();

        return $repairerData;
    }
}
