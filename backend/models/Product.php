<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\db\mssql;
use yii\db\Query;

class Product extends Model
{
    /**
     * @var int
     */
    private $numberOfPages = 5;

    /**
     * @var int
     */
    private $limit = 5;

    /**
     * @var array
     */
    private $countries = array(
        1 => 'Amerika',
        2 => 'Almaniya',
        3 => 'Fransa',
        4 => 'Danimarka',
        5 => 'Ukrayina',
        6 => 'Rusiya',
        7 => 'Norveç',
        8 => 'İsvecrə',
        9 => 'İrlandiya',
        10 => 'Polşa',
    );

    /**
     * @var array
     */
    private $cities = array(
        1 => 'Astara',
        2 => 'Ağcabədi',
        3 => 'Ağdam',
        4 => 'Ağdaş',
        5 => 'Ağdərə',
        6 => 'Ağstafa',
        7 => 'Ağsu',
        8 => 'Bakı',
        9 => 'Balakən',
        10 => 'Beyləqan',
        11 => 'Biləsuvar',
        12 => 'Bərdə',
        13 => 'Culfa',
        14 => 'Cəbrayıl',
        15 => 'Cəlilabad',
        16 => 'Daşkəsən',
        17 => 'Dəliməmmədli',
        18 => 'Füzuli',
        19 => 'Goranboy',
        20 => 'Göygöl',
        21 => 'Göytəpə',
        22 => 'Göyçay',
        23 => 'Gədəbəy',
        24 => 'Gəncə',
        25 => 'Horadiz',
        26 => 'Kürdəmir',
        27 => 'Kəlbəcər',
        28 => 'Laçın',
        29 => 'Lerik',
        30 => 'Liman',
        31 => 'Lənkəran',
        32 => 'Masallı',
        33 => 'Mingəçevir',
        34 => 'Naftalan',
        35 => 'Naxçıvan',
        36 => 'Neftçala',
        37 => 'Ordubad',
        38 => 'Oğuz',
        39 => 'Qax',
        40 => 'Qazax',
        41 => 'Qobustan',
        42 => 'Quba',
        43 => 'Qubadlı',
        44 => 'Qusar',
        45 => 'Qəbələ',
        46 => 'Saatlı',
        47 => 'Sabirabad',
        48 => 'Salyan',
        49 => 'Samux',
        50 => 'Siyəzən',
        51 => 'Sumqayıt',
        52 => 'Tovuz',
        53 => 'Tərtər',
        54 => 'Ucar',
        55 => 'Xankəndi',
        56 => 'Xaçmaz',
        57 => 'Xocalı',
        58 => 'Xocavənd',
        59 => 'Xudat',
        60 => 'Xırdalan',
        61 => 'Xızı',
        62 => 'Yardımlı',
        63 => 'Yevlax',
        64 => 'Zaqatala',
        65 => 'Zəngilan',
        66 => 'Zərdab',
        67 => 'İmişli',
        68 => 'İsmayıllı',
        69 => 'Şabran',
        70 => 'Şahbuz',
        71 => 'Şamaxı',
        72 => 'Şirvan',
        73 => 'Şuşa',
        74 => 'Şəki',
        75 => 'Şəmkir',
        76 => 'Şərur',
    );

    /**
     * @var array
     */
    private $colors = array(
        1 => 'Ağ',
        2 => 'Bej',
        3 => 'Boz',
        4 => 'Bənövşəyi',
        5 => 'Göy',
        6 => 'Gümüşü',
        7 => 'Mavi',
        8 => 'Narıncı',
        9 => 'Qara',
        10 => 'Qırmızı',
        11 => 'Qızılı',
        12 => 'Qəhvəyi',
        13 => 'Sarı',
        14 => 'Tünd qırmız',
        15 => 'Yaş Asfalt',
        16 => 'Yaşıl',
        17 => 'Çəhrayı',
    );

    /**
     * @var array
     */
    private $productCategoryType = array(
        1 => '#interior',
        2 => '#exterior',
        3 => '#action-part',
        4 => '#general',
    );

    /**
     * @var array
     */
    private $carList = array(
        1 => array(
            'brand' => 'Audi',
            'model' => array(
                1 => '100',
                2 => 'A1',
                3 => 'A3',
                4 => 'A4',
                5 => 'A5',
                6 => 'A6',
                7 => 'A7',
                8 => 'A8',
                9 => 'Allroad',
                10 => 'Q3',
                11 => 'Q5',
                12 => 'Q7',
                13 => 'R8',
                14 => 'RS7',
                15 => 'S4',
                16 => 'S5',
                17 => 'S7',
                18 => 'S8',
                19 => 'TT',
                20 => 'TTS',
            ),
        ),

        2 => array(
            'brand' => 'Acura',
            'model' => array(
                1 => 'ILX',
                2 => 'MDX',
            ),
        ),

        3 => array(
            'brand' => 'Alfa Romeo',
            'model' => array(
                1 => '156',
                2 => '164',
            ),
        ),

        4 => array(
            'brand' => 'Aston Martin',
            'model' => array(
                1 => 'DB9',
                2 => 'DBS',
                3 => 'Rapide',
            ),
        ),

        5 => array(
            'brand' => 'ABM',
            'model' => array(
                1 => 'Volcan 150',
            ),
        ),

        6 => array(
            'brand' => 'ACG Cars',
            'model' => array(
                1 => 'Cadillac Escalade',
                2 => 'California Roadster',
                3 => 'Hummer H3',
                4 => 'Roadster E39',
                5 => 'T Sport',
            ),
        ),

        7 => array(
            'brand' => 'Arctic Cat',
            'model' => array(
                1 => 'Competition MUDPRO 700 Limited EPS',
                2 => 'Mid Size 500',
                3 => 'TRV 700 Limited EPS',
                4 => 'XR 550 XT EPS',
            ),
        ),

        8 => array(
            'brand' => 'BMC',
            'model' => array(
                1 => 'Pro 827',
            ),
        ),

        9 => array(
            'brand' => 'BMW',
            'model' => array(
                1 => '116',
                2 => '118',
                3 => '120',
                4 => '316',
                5 => '318',
                6 => '320',
                7 => '321',
                8 => '323',
                9 => '325',
                10 => '328',
                11 => '330',
                12 => '335',
                13 => '520',
                14 => '523',
                15 => '525',
                16 => '528',
                17 => '530',
                18 => '535',
                19 => '540',
                20 => '545',
                21 => '550',
                22 => '728',
                23 => '730',
                24 => '735',
                25 => '740',
                26 => '745',
                27 => '750',
                28 => '760',
                29 => '640',
                30 => '645',
                31 => '650',
                32 => 'Z4',
                33 => 'M3',
                34 => 'M5',
                35 => 'M6',
                36 => 'X5 M',
                37 => 'X6 M',
                38 => 'X1',
                39 => 'X3',
                40 => 'X4',
                41 => 'X5',
                42 => 'X6',
                43 => 'S 1000 RR',
                44 => '220i',
            ),
        ),

        10 => array(
            'brand' => 'BMW Alpina',
            'model' => array(
                1 => 'B10',
                2 => 'B5',
                3 => 'B7',
            ),
        ),

        11 => array(
            'brand' => 'BYD',
            'model' => array(
                1 => 'F3',
            ),
        ),

        12 => array(
            'brand' => 'Baic',
            'model' => array(
                1 => 'A115',
                2 => 'BJ40',
            ),
        ),

        13 => array(
            'brand' => 'Bentley',
            'model' => array(
                1 => 'Continental',
                2 => 'Flying Spur',
                3 => 'Mulsanne',
            ),
        ),

        14 => array(
            'brand' => 'Brilliance',
            'model' => array(
                1 => 'H 530',
                2 => 'V5',
            ),
        ),

        15 => array(
            'brand' => 'Buick',
            'model' => array(
                1 => 'Enclave',
                2 => 'LaCrosse',
            ),
        ),

        16 => array(
            'brand' => 'Cadillac',
            'model' => array(
                1 => 'BLS',
                2 => 'CTS',
                3 => 'CTS-V',
                4 => 'Escalade',
                5 => 'SRX',
            ),
        ),

        17 => array(
            'brand' => 'Can-Am',
            'model' => array(
                1 => 'Commander',
                2 => 'Spyder RS',
            ),
        ),

        18 => array(
            'brand' => 'Chana',
            'model' => array(
                1 => 'Benni',
                2 => 'CM8',
            ),
        ),

        19 => array(
            'brand' => 'Changan',
            'model' => array(
                1 => 'Alsvin',
                2 => 'Benni',
                3 => 'CS 35',
                4 => 'CS 75',
                5 => 'CX 20',
                6 => 'EU-Love',
                7 => 'Eado',
                8 => 'Honor',
                9 => 'Love',
                10 => 'Mini',
                11 => 'Raeton',
            ),
        ),

        20 => array(
            'brand' => 'Chery',
            'model' => array(
                1 => 'A-15 Cowin',
                2 => 'Arrizo 7',
                3 => 'E5',
                4 => 'Eastar',
                5 => 'Fora',
                6 => 'Fulwin 2',
                7 => 'QQ',
                8 => 'Tiggo',
                9 => 'Tiggo 5',
            ),
        ),

        21 => array(
            'brand' => 'Chevrolet',
            'model' => array(
                1 => 'Alero',
                2 => 'Aveo',
                3 => 'Camaro',
                4 => 'Captiva',
                5 => 'Cavalier',
                6 => 'Cobalt',
                7 => 'Cruze',
                8 => 'Epica',
                9 => 'Equinox',
                10 => 'Lacetti',
                11 => 'Lumina',
                12 => 'Malibu',
                13 => 'Niva',
                14 => 'Nubira',
                15 => 'Orlando',
                16 => 'Tahoe',
                17 => 'TrailBlazer',
                18 => 'Traverse',
                19 => 'Trax',
            ),
        ),

        22 => array(
            'brand' => 'Chrysler',
            'model' => array(
                1 => '300C',
                2 => 'Acclaim',
                3 => 'Concorde',
                4 => 'Crossfire',
                5 => 'Neon',
                6 => 'PT Cruiser',
                7 => 'Pacifica',
                8 => 'Prowler',
                9 => 'Sebring',
                10 => 'Stratus',
                11 => 'Town & Country',
                12 => 'Voyager',
            ),
        ),

        23 => array(
            'brand' => 'Citroen',
            'model' => array(
                1 => 'Berlingo VP',
                2 => 'C-Elysee',
                3 => 'C3',
                4 => 'C3 Picasso',
                5 => 'C4',
                6 => 'C5',
                7 => 'DS3',
                8 => 'DS4',
                9 => 'DS5',
                10 => 'Xsara',
            ),
        ),

        24 => array(
            'brand' => 'DAF',
            'model' => array(
                1 => '105XF',
                2 => '85CF',
                3 => '95XF',
            ),
        ),

        25 => array(
            'brand' => 'Dacia',
            'model' => array(
                1 => 'Logan',
            ),
        ),

        26 => array(
            'brand' => 'Daewoo',
            'model' => array(
                1 => 'BS090',
                2 => 'Damas',
                3 => 'Espero',
                5 => 'Gentra',
                6 => 'Lanos',
                7 => 'Leganza',
                8 => 'Matiz',
                9 => 'Nexia',
                10 => 'Nubira',
                11 => 'Prince',
                12 => 'Racer',
                13 => 'Super Salon',
                14 => 'Tico',
            ),
        ),

        27 => array(
            'brand' => 'Daihatsu',
            'model' => array(
                1 => 'Cuore',
                2 => 'Materia',
                3 => 'Terios',
            ),
        ),

        28 => array(
            'brand' => 'Dayun',
            'model' => array(
                1 => 'DY125-B',
                2 => 'DY150-12',
                3 => 'DY150GY',
            ),
        ),

        29 => array(
            'brand' => 'Dnepr',
            'model' => array(
                1 => '11',
                2 => 'K-750',
            ),
        ),

        30 => array(
            'brand' => 'DongFeng',
            'model' => array(
                1 => 'Fengshen H30',
                2 => 'Fengshen S30',
            ),
        ),

        31 => array(
            'brand' => 'Ducati',
            'model' => array(
                1 => '1100 EVO',
                2 => '1199 Panigale',
                3 => '1199 Panigale S',
                4 => '696',
                5 => '848 EVO',
                6 => '848 EVO Corse',
                7 => '899 Panigale',
                8 => 'Diavel',
                9 => 'Scrambler',
                10 => 'Streetfighter 1098 S',
            ),
        ),

        32 => array(
            'brand' => 'FAUN',
            'model' => array(
                1 => 'ATF 65 G-4',
            ),
        ),

        33 => array(
            'brand' => 'FAW',
            'model' => array(
                1 => 'CA1010',
                2 => 'CA102105',
                3 => 'Besturn B 90',
            ),
        ),

        34 => array(
            'brand' => 'Fiat',
            'model' => array(
                1 => '500',
                2 => 'Albea',
                3 => 'Bravo',
                4 => 'Cinquecento',
                5 => 'Doblo',
                6 => 'Fiorino',
                7 => 'Linea',
                8 => 'Punto',
                9 => 'Qubo',
            ),
        ),

        35 => array(
            'brand' => 'Ford',
            'model' => array(
                1 => 'Bronco',
                2 => 'C-Max',
                3 => 'Cargo',
                4 => 'Courier',
                5 => 'Edge',
                6 => 'Escape',
                7 => 'Escort',
                8 => 'Expedition',
                9 => 'Explorer',
                10 => 'Fiesta',
                11 => 'Five Hundred',
                12 => 'Focus',
                13 => 'Fusion',
                14 => 'Galaxy',
                15 => 'Kuga',
                16 => 'Mercury',
                17 => 'Mondeo',
                18 => 'Mustang',
                19 => 'Scorpio',
                20 => 'Sierra',
                21 => 'Tourneo Connect',
                22 => 'Transit',
            ),
        ),

        36 => array(
            'brand' => 'Foton',
            'model' => array(
                1 => 'Forland',
            ),
        ),

        37 => array(
            'brand' => 'GAC',
            'model' => array(
                1 => 'GA 5',
                2 => 'GS 5',
            ),
        ),

        38 => array(
            'brand' => 'GEM Cars',
            'model' => array(
                1 => 'E4',
                2 => 'E6',
                3 => 'EL',
            ),
        ),

        39 => array(
            'brand' => 'GMC',
            'model' => array(
                1 => 'Acadia',
                2 => 'Savana',
                3 => 'Terrain',
                4 => 'Yukon',
            ),
        ),

        40 => array(
            'brand' => 'Gabro',
            'model' => array(
                1 => '125 RZ-6',
                2 => 'Sport G 250R',
                3 => 'ZERO250-RZ',
            ),
        ),

        41 => array(
            'brand' => 'Gaz',
            'model' => array(
                1 => '12',
                2 => '13',
                3 => '24',
                4 => '24-02',
                5 => '24-10',
                6 => '2705-Gazel',
                7 => '3102',
                8 => '31029',
                9 => '3110',
                10 => '31105',
                11 => '3221',
                12 => '3302-Gazel',
                13 => '3307',
                14 => '52',
                15 => '53',
                16 => '66',
                17 => '69',
                18 => 'M-1',
                19 => 'M-20 Pobeda',
                20 => 'M-21',
                21 => 'Next',
                22 => 'Siber',
                23 => 'Sobol',
            ),
        ),

        42 => array(
            'brand' => 'Geely',
            'model' => array(
                1 => 'Emgrand 7',
                2 => 'MK',
            ),
        ),

        43 => array(
            'brand' => 'Ginaf',
            'model' => array(
                1 => 'M4243',
            ),
        ),

        44 => array(
            'brand' => 'Great Wall',
            'model' => array(
                1 => 'Haval H-3',
                2 => 'Haval H-5-E',
                3 => 'Haval H-5-T',
                4 => 'Haval H-6',
                5 => 'Haval M2',
                6 => 'Haval M4',
                7 => 'Hover',
                8 => 'Voleex C-30',
                9 => 'Wingle-5',
            ),
        ),

        45 => array(
            'brand' => 'HOWO',
            'model' => array(
                1 => 'A7',
                2 => 'Sinotruk',
            ),
        ),

        46 => array(
            'brand' => 'Haojue',
            'model' => array(
                1 => 'HJ110-2C',
                2 => 'HJ125-11A',
                3 => 'HJ125-16C',
                4 => 'HJ125-7D',
                5 => 'HJ125-8C',
            ),
        ),

        47 => array(
            'brand' => 'Night Rod',
            'model' => array(
                1 => 'Sportster Custom 1200',
                2 => 'XL883N Sportster Iron 883',
                3 => 'Night Rod',
            ),
        ),

        48 => array(
            'brand' => 'Haval',
            'model' => array(
                1 => 'H2',
                2 => 'H6 Sport',
                3 => 'H8',
                4 => 'H9',
            ),
        ),

        49 => array(
            'brand' => 'Honda',
            'model' => array(
                1 => 'CG 125',
                2 => 'Dio AF',
                3 => 'Dio SR',
                4 => 'Today',
                5 => 'Accord',
                6 => 'CR-V',
                7 => 'City',
                8 => 'Civic',
                9 => 'Odyssey',
                10 => 'Pilot',
            ),
        ),

        50 => array(
            'brand' => 'Hummer',
            'model' => array(
                1 => 'H2',
                2 => 'H3',
            ),
        ),

        51 => array(
            'brand' => 'Hyundai',
            'model' => array(
                1 => 'Accent',
                2 => 'Azera',
                3 => 'County',
                4 => 'Coupe',
                5 => 'Elantra',
                6 => 'Equus',
                7 => 'Excel',
                8 => 'Galloper',
                9 => 'Genesis',
                10 => 'Getz',
                11 => 'Grand Santa Fe',
                12 => 'Grandeur',
                13 => 'H 100',
                14 => 'H-1',
                15 => 'HD-45',
                16 => 'HD-65',
                17 => 'HD-78',
                18 => 'Matrix',
                19 => 'Santa Fe',
                20 => 'Solaris',
                21 => 'Sonata',
                22 => 'Super Aero City',
                23 => 'Terracan',
                24 => 'Tiburon',
                25 => 'Tucson',
                26 => 'Veloster',
                27 => 'Veracruz',
                28 => 'i10',
                29 => 'i20',
                30 => 'i30',
                31 => 'i40',
                32 => 'ix20',
                33 => 'ix35',
                34 => 'ix55',
            ),
        ),

        52 => array(
            'brand' => 'IJ',
            'model' => array(
                1 => 'Planeta 5',
                2 => 'Upiter-4',
                3 => '2717',
                4 => '27175',
                5 => 'Planeta 4',
            ),
        ),

        53 => array(
            'brand' => 'Infiniti',
            'model' => array(
                1 => 'G25',
                2 => 'G35',
                3 => 'G37',
                4 => 'G37S',
                5 => 'M35',
                6 => 'M37',
                7 => 'EX25',
                8 => 'EX35',
                9 => 'FX35',
                10 => 'FX37',
                11 => 'FX45',
                12 => 'FX50',
                13 => 'JX35',
                14 => 'I30',
                15 => 'Q70',
                16 => 'QX4',
                17 => 'QX56',
                18 => 'QX60',
                19 => 'QX70',
            ),
        ),

        54 => array(
            'brand' => 'Iran Khodro',
            'model' => array(
                1 => 'Azsamand',
                2 => 'Runna',
                3 => 'Soren',
            ),
        ),

        55 => array(
            'brand' => 'Isuzu',
            'model' => array(
                1 => 'MD 22 B',
                2 => 'MD 27',
                3 => 'NHR 55 E',
                4 => 'NKR 55 L',
                5 => 'NPR 58',
                6 => 'NPR 66 L',
                7 => 'NPR 75',
                8 => 'Turkuaz',
                9 => 'Urban',
            ),
        ),

        56 => array(
            'brand' => 'Iveco',
            'model' => array(
                1 => '35 S11',
                2 => 'Astra HD8',
                3 => 'Daily 35C15VH',
                4 => 'EuroStar',
                5 => 'Eurocargo',
                6 => 'Eurotrakker Cursor',
                7 => 'Otoyol',
                8 => 'Stralis',
            ),
        ),

        58 => array(
            'brand' => 'JAC',
            'model' => array(
                1 => 'HFC 1040K',
                2 => 'HFC 3090K',
                3 => 'HFC1020',
                4 => 'J5',
            ),
        ),

        59 => array(
            'brand' => 'JMC',
            'model' => array(
                1 => 'JX1062',
            ),
        ),

        60 => array(
            'brand' => 'Jaguar',
            'model' => array(
                1 => 'XE',
                2 => 'XF',
                3 => 'XFR',
                4 => 'XJ',
                5 => 'S-Type',
            ),
        ),

        61 => array(
            'brand' => 'Jeep',
            'model' => array(
                1 => 'Cherokee',
                2 => 'Compass',
                3 => 'Grand Cherokee',
                4 => 'Patriot',
                5 => 'Wrangler',
            ),
        ),

        62 => array(
            'brand' => 'Jinbei',
            'model' => array(
                1 => 'Granse',
            ),
        ),

        63 => array(
            'brand' => 'KAvZ',
            'model' => array(
                1 => '3270',
            ),
        ),

        64 => array(
            'brand' => 'KamAz',
            'model' => array(
                1 => '43101',
                2 => '43253',
                3 => '44108',
                4 => '5320',
                5 => '53202',
                6 => '53212',
                7 => '53213',
                8 => '53215',
                9 => '53229',
                10 => '5410',
                11 => '54112',
                12 => '54115',
                13 => '5511',
                14 => '55111',
                15 => '55114',
                16 => '65115',
                17 => '6520',
                18 => '65222',
            ),
        ),

        65 => array(
            'brand' => 'Kawasaki',
            'model' => array(
                1 => 'VN 1600 Mean Streak',
                2 => 'Z1000',
            ),
        ),

        66 => array(
            'brand' => 'Kia',
            'model' => array(
                1 => 'Bongo',
                2 => 'Cadenza',
                3 => 'Capital',
                4 => 'Carens',
                5 => 'Carnival',
                6 => 'Cee`d',
                7 => 'Cerato',
                8 => 'Clarus',
                9 => 'K2700',
                10 => 'Koup',
                11 => 'Magentis',
                12 => 'Mohave',
                13 => 'Opirus',
                14 => 'Optima',
                15 => 'Picanto',
                16 => 'Pride',
                17 => 'Quoris',
                18 => 'Rio',
                19 => 'Shuma',
                20 => 'Sorento',
                21 => 'Soul',
                22 => 'Sportage',
            ),
        ),

        67 => array(
            'brand' => 'Land Rover',
            'model' => array(
                1 => 'Defender',
                2 => 'Discovery',
                3 => 'Discovery Sport',
                4 => 'Freelander',
                5 => 'RR Evoque',
                6 => 'RR Sport',
                7 => 'Range Rover',
            ),
        ),

        68 => array(
            'brand' => 'Lexus',
            'model' => array(
                1 => 'IS 250',
                2 => 'IS 300',
                3 => 'IS 350',
                4 => 'GS 250',
                5 => 'GS 300',
                6 => 'GS 350',
                7 => 'ES 250',
                8 => 'ES 350',
                9 => 'LS 430',
                10 => 'LS 460',
                11 => 'LS 600 H',
                12 => 'RX 300',
                13 => 'RX 330',
                14 => 'RX 350',
                15 => 'RX 400 H',
                16 => 'GX 460',
                17 => 'LX 470',
                18 => 'LX 570',
                19 => 'NX 200T',
                20 => 'NX 300H',
            ),
        ),

        69 => array(
            'brand' => 'Lifan',
            'model' => array(
                1 => '320',
                2 => '520',
                3 => '620',
                4 => '720',
                5 => 'X60',
            ),
        ),

        70 => array(
            'brand' => 'Lincoln',
            'model' => array(
                1 => 'Aviator',
                2 => 'Continental',
                3 => 'MKZ',
                4 => 'Towncar',
            ),
        ),

        71 => array(
            'brand' => 'Lotus',
            'model' => array(
                1 => 'Evora S',
            ),
        ),

        72 => array(
            'brand' => 'LuAz',
            'model' => array(
                1 => '969M',
            ),
        ),

        73 => array(
            'brand' => 'MAN',
            'model' => array(
                1 => '19463',
                2 => '22.321',
                3 => '26463',
                4 => '32.270',
                5 => '8-163',
                6 => 'Cobra Guleryuz',
                7 => 'F2000',
                8 => 'L2000',
                9 => 'TGA 18.310',
                10 => 'TGA 18.410',
                11 => 'TGA 18.430',
                12 => 'TGA 18.480',
                13 => 'TGA 26.430',
                14 => 'TGA 33350',
                15 => 'TGA 390',
                16 => 'TGA 41.400',
                17 => 'TGA 510',
                18 => 'TGS 33.360',
                19 => 'TGS 41.400',
            ),
        ),

        74 => array(
            'brand' => 'MAZ',
            'model' => array(
                1 => '3577',
                2 => '5337',
                3 => '642208',
                4 => '64229',
            ),
        ),

        75 => array(
            'brand' => 'MG',
            'model' => array(
                1 => '3',
                2 => '350',
                3 => '550',
                4 => '6',
            ),
        ),

        76 => array(
            'brand' => 'Maserati',
            'model' => array(
                1 => 'Ghibli',
                2 => 'GranCabrio',
                3 => 'GranTurismo',
                4 => 'Quattroporte',
            ),
        ),

        77 => array(
            'brand' => 'Mazda',
            'model' => array(
                1 => '2',
                2 => '3',
                3 => '6',
                4 => '626',
                5 => 'CX-5',
                6 => 'CX-7',
                7 => 'CX-9',
                8 => 'MPV',
                9 => 'RX 8',
                10 => 'Tribute',
            ),
        ),

        78 => array(
            'brand' => 'Megelli',
            'model' => array(
                1 => 'Naked',
                2 => 'Sport',
                3 => 'Sport SE',
            ),
        ),

        79 => array(
            'brand' => 'Mercedes',
            'model' => array(
                1 => 'A 140',
                2 => 'A 160',
                3 => 'A 170',
                4 => 'A 180',
                5 => 'A 190',
                6 => 'A 200',
                7 => 'C 180',
                8 => 'C 200',
                9 => 'C 220',
                10 => 'C 230',
                11 => 'C 240',
                12 => 'C 250',
                13 => 'C 270',
                14 => 'C 280',
                15 => 'C 300',
                16 => 'C 32 AMG',
                17 => 'C 320',
                18 => 'C 350',
                19 => 'C 36 AMG',
                20 => 'C 43 AMG',
                21 => 'C 63 AMG',
                22 => '230 CE',
                23 => 'E 200',
                24 => 'E 220',
                25 => 'E 230',
                26 => 'E 240',
                27 => 'E 250',
                28 => 'E 260',
                29 => 'E 270',
                30 => 'E 280',
                31 => 'E 290',
                32 => 'E 300',
                33 => 'E 320',
                34 => 'E 350',
                35 => 'E 400',
                36 => 'E 420',
                37 => 'E 430',
                38 => 'E 55 AMG',
                39 => 'E 550',
                40 => 'E 63 AMG',
                41 => 'G 230',
                42 => 'G 270',
                43 => 'G 300',
                44 => 'G 320',
                45 => 'G 350',
                46 => 'G 400',
                47 => 'G 500',
                48 => 'G 55 AMG',
                49 => 'G 63 AMG',
                50 => 'G 65 AMG',
                51 => 'B 170',
                52 => 'B 180',
                53 => 'B 200',
                54 => 'CL 500',
                55 => 'CL 55 AMG',
                56 => 'CL 600',
                57 => 'CL 63 AMG',
                58 => 'CL 65 AMG',
                59 => 'CLS 350',
                60 => 'CLS 500',
                61 => 'CLS 55 AMG',
                62 => 'CLS 63 AMG',
                63 => 'GL 320',
                64 => 'GL 350 CDI',
                65 => 'GL 400',
                66 => 'GL 450',
                67 => 'GL 500',
                68 => 'GL 550',
                69 => 'GL 63 AMG',
                70 => 'GL Brabus Widestar',
                71 => 'GLK 250',
                72 => 'GLK 280',
                73 => 'GLK 300',
                74 => 'GLK 350',
                75 => 'ML 230',
                76 => 'ML 270',
                77 => 'ML 280',
                78 => 'ML 300',
                79 => 'ML 320',
                80 => 'ML 350',
                81 => 'ML 400',
                82 => 'ML 500',
                83 => 'ML 550',
                84 => 'ML 63 AMG',
                85 => 'R 350',
                86 => '280 SE',
                87 => '300 SD',
                88 => '500 SEL',
                89 => '560 SEL',
                90 => 'S 300',
                91 => 'S 320',
                92 => 'S 350',
                93 => 'S 400',
                94 => 'S 420',
                95 => 'S 430',
                96 => 'S 500',
                97 => 'S 55 AMG',
                98 => 'S 550',
                99 => 'S 600',
                100 => 'S 63 AMG',
                101 => 'S 65 AMG',
                102 => 'SL 55 AMG',
                103 => 'SLK 200',
                104 => 'SLK 230',
                105 => 'SLK 320',
                106 => 'SLK 350',
                107 => 'Vaneo',
                108 => 'Viano',
                109 => 'Vito',
                110 => 'CLK 200',
                111 => 'CLK 230',
                112 => 'CLK 240',
                113 => 'CLK 280',
                114 => 'CLK 320',
                115 => 'CLA 200',
                116 => 'CLA 220',
                117 => 'CLA 250',
                118 => '0303',
                119 => '0403',
                120 => '208 D',
                121 => '308 D',
                122 => '312 D',
                123 => '408 D',
                124 => '409 D',
                125 => '410 D',
                126 => '609 D',
                127 => '612 D',
                128 => '709 D',
                129 => '711 D',
                130 => '809 D',
                131 => '811 D',
                132 => '812 D',
                133 => '813',
                134 => '814 D',
                135 => 'Actros 1831',
                136 => 'Actros 1840',
                137 => 'Actros 1843',
                138 => 'Actros 1844',
                139 => 'Actros 1848',
                140 => 'Actros 2540',
                141 => 'Actros 2635',
                142 => 'Actros 3236',
                143 => 'Actros 4140',
                144 => 'Actros 4143',
                145 => 'Atego 1217',
                146 => 'Atego 1218',
                147 => 'Atego 1223',
                148 => 'Atego 1224',
                149 => 'Atego 1228',
                150 => 'Atego 1317',
                151 => 'Atego 1318',
                152 => 'Atego 1323',
                153 => 'Atego 1517',
                154 => 'Atego 1518',
                155 => 'Atego 1523',
                156 => 'Atego 1524',
                157 => 'Atego 1528',
                158 => 'Atego 1823',
                159 => 'Atego 1828',
                160 => 'Atego 2528',
                161 => 'Atego 812',
                162 => 'Atego 814',
                163 => 'Atego 815',
                164 => 'Atego 816',
                165 => 'Atego 817',
                166 => 'Atego 818',
                167 => 'Atego 917',
                168 => 'Axor 1835',
                169 => 'Axor 1840',
                170 => 'Axor 1843',
                171 => 'Axor 2528',
                172 => 'Axor 3240',
                173 => 'Axor 4140',
                174 => 'Vario',
                175 => 'Sprinter 208',
                176 => 'Sprinter 210',
                177 => 'Sprinter 224',
                178 => 'Sprinter 308',
                179 => 'Sprinter 311',
                180 => 'Sprinter 312',
                181 => 'Sprinter 313',
                182 => 'Sprinter 314',
                183 => 'Sprinter 315',
                184 => 'Sprinter 316',
                185 => 'Sprinter 412',
                186 => 'Sprinter 413',
                187 => 'Sprinter 416',
                188 => 'Sprinter 513',
                189 => 'Sprinter 515',
                190 => 'Sprinter 516',
                191 => 'GLA 250',
                192 => 'GLE 400',
                193 => 'GLE 450 AMG Coupe',
                194 => '190',
                195 => '200',
                196 => 'AMG GT',
            ),
        ),

        80 => array(
            'brand' => 'Mercury',
            'model' => array(
                1 => 'Mountaineer',
            ),
        ),

        81 => array(
            'brand' => 'Mini',
            'model' => array(
                1 => 'Cooper',
                2 => 'Countryman',
            ),
        ),

        82 => array(
            'brand' => 'Mitsubishi',
            'model' => array(
                1 => 'ASX',
                2 => 'Airtrek',
                3 => 'Attrage',
                4 => 'Canter',
                5 => 'Carisma',
                6 => 'Chariot',
                7 => 'Fuso Canter',
                8 => 'Galant',
                9 => 'Grandis',
                10 => 'L 200',
                11 => 'L 300',
                12 => 'Lancer',
                13 => 'Magna',
                14 => 'Mirage',
                15 => 'Montero',
                16 => 'Nativa',
                17 => 'Outlander',
                18 => 'Pajero',
                19 => 'Pajero Pinin',
                20 => 'Pajero io',
                21 => 'Rosa',
            ),
        ),

        83 => array(
            'brand' => 'Moskvich',
            'model' => array(
                1 => '2140',
                2 => '2141',
                3 => '400',
                4 => '402',
                5 => '407',
                6 => '408',
                7 => '412',
            ),
        ),

        84 => array(
            'brand' => 'Muravey',
            'model' => array(
                1 => 'TGA200',
            ),
        ),

        85 => array(
            'brand' => 'Nissan',
            'model' => array(
                1 => 'Altima',
                2 => 'Bluebird',
                3 => 'March',
                4 => 'Armada',
                5 => 'Maxima',
                6 => 'Micra',
                7 => 'Murano',
                8 => 'Navara',
                9 => 'Pathfinder',
                10 => 'Patrol',
                11 => 'Presage',
                12 => 'Primera',
                13 => 'Qashqai',
                14 => 'Rogue',
                15 => 'Sentra',
                16 => 'Sunny',
                17 => 'Teana',
                18 => 'Terrano',
                19 => 'Tiida',
                20 => 'Urvan',
                21 => 'X-Trail',
                22 => 'Xterra',
            ),
        ),

        86 => array(
            'brand' => 'Opel',
            'model' => array(
                1 => 'Antara',
                2 => 'Combo',
                3 => 'Corsa',
                4 => 'Astra',
                5 => 'Frontera',
                6 => 'Insignia',
                7 => 'Meriva',
                8 => 'Omega',
                9 => 'Signum',
                10 => 'Sintra',
                11 => 'Tigra',
                12 => 'Vectra',
                13 => 'Vita',
                14 => 'Zafira',
            ),
        ),

        87 => array(
            'brand' => 'PAZ',
            'model' => array(
                1 => '3204',
                2 => '32054',
                3 => 'Vektor 3',
            ),
        ),

        88 => array(
            'brand' => 'Peugeot',
            'model' => array(
                1 => '206',
                2 => '207',
                3 => '3008',
                4 => '301',
                5 => '307',
                6 => '308',
                7 => '308 CC',
                8 => '405',
                9 => '406',
                10 => '407',
                11 => '5008',
                12 => '508',
                13 => '607',
                14 => 'Pars',
                15 => 'Partner',
                16 => 'RCZ',
            ),
        ),

        89 => array(
            'brand' => 'Polaris',
            'model' => array(
                1 => 'RZR 900 XC',
                2 => 'RZR XP 1000',
                3 => 'Slingshot',
                4 => 'Sportsman Touring 570',
            ),
        ),

        90 => array(
            'brand' => 'Porsche',
            'model' => array(
                1 => '911',
                2 => 'Boxster',
                3 => 'Cayenne',
                4 => 'Macan',
                5 => 'Panamera',
            ),
        ),

        91 => array(
            'brand' => 'RAF',
            'model' => array(
                1 => '22031',
            ),
        ),

        92 => array(
            'brand' => 'Renault',
            'model' => array(
                1 => '11',
                2 => '18',
                3 => '19',
                4 => '21',
                5 => 'Clio',
                6 => 'Kangoo',
                7 => 'Koleos',
                8 => 'Logan',
                9 => 'Megane',
                10 => 'Safrane',
                11 => 'Sandero',
                12 => 'Symbol',
            ),
        ),

        93 => array(
            'brand' => 'Rieju',
            'model' => array(
                1 => 'RS3 185',
            ),
        ),

        94 => array(
            'brand' => 'Rolls-Royce',
            'model' => array(
                1 => 'Ghost',
                2 => 'Phantom',
            ),
        ),

        95 => array(
            'brand' => 'Rover',
            'model' => array(
                1 => '200',
                2 => '600',
                3 => '75',
            ),
        ),

        96 => array(
            'brand' => 'SEAT',
            'model' => array(
                1 => 'Altea',
                2 => 'Exeo',
                3 => 'Ibiza',
                4 => 'Leon',
                5 => 'Toledo',
            ),
        ),

        97 => array(
            'brand' => 'Saab',
            'model' => array(
                1 => '9-3',
            ),
        ),

        98 => array(
            'brand' => 'Saipa',
            'model' => array(
                1 => '132',
                2 => '141',
                3 => 'Tiba',
            ),
        ),

        99 => array(
            'brand' => 'Saturn',
            'model' => array(
                1 => 'Aura',
                2 => 'VUE',
            ),
        ),

        100 => array(
            'brand' => 'Scion',
            'model' => array(
                1 => 'xB',
            ),
        ),

        101 => array(
            'brand' => 'Shacman',
            'model' => array(
                1 => 'F2000',
                2 => 'F3000',
                3 => 'SX3314DT366',
                4 => 'SX3315DT366',
            ),
        ),

        102 => array(
            'brand' => 'Shaolin',
            'model' => array(
                1 => 'SLG 6602',
            ),
        ),

        103 => array(
            'brand' => 'Shineray',
            'model' => array(
                1 => 'XY 250',
                2 => 'XY250GY',
            ),
        ),

        104 => array(
            'brand' => 'Skoda',
            'model' => array(
                1 => 'Fabia',
                2 => 'Octavia',
                3 => 'Praktik',
                4 => 'Rapid',
                5 => 'Yeti',
            ),
        ),

        105 => array(
            'brand' => 'Smart',
            'model' => array(
                1 => 'Fortwo',
                2 => 'Roadster',
            ),
        ),

        106 => array(
            'brand' => 'Ssang Yong',
            'model' => array(
                1 => 'Actyon',
                2 => 'Korando',
                3 => 'Kyron',
                4 => 'Rexton',
                5 => 'Tivoli',
            ),
        ),

        107 => array(
            'brand' => 'Subaru',
            'model' => array(
                1 => 'B9 Tribeca',
                2 => 'Forester',
                3 => 'Impreza',
                4 => 'Legacy',
                5 => 'Tivoli',
                6 => 'Outback',
                7 => 'Tribeca',
                8 => 'XV',
            ),
        ),

        108 => array(
            'brand' => 'Suzuki',
            'model' => array(
                1 => 'AN 400',
                2 => 'GSF 250 Bandit',
                3 => 'GSX-R 1000',
                4 => 'Grand Vitara',
                5 => 'Ignis',
                6 => 'Jimny',
                7 => 'Kizashi',
                8 => 'Reno',
                9 => 'SX4',
                10 => 'Swift',
                11 => 'Vitara',
                12 => 'XL7',
            ),
        ),

        109 => array(
            'brand' => 'Tesla',
            'model' => array(
                1 => 'Model S',
            ),
        ),

        110 => array(
            'brand' => 'Tofas',
            'model' => array(
                1 => 'Dogan',
                2 => 'Kartal',
                3 => 'Sahin',
            ),
        ),

        111 => array(
            'brand' => 'Toyota',
            'model' => array(
                1 => '4-Runner',
                2 => 'Avalon',
                3 => 'Camry',
                4 => 'Celica',
                5 => 'Corolla',
                6 => 'Corolla Verso',
                7 => 'Crown',
                8 => 'FJ Cruiser',
                9 => 'Fortuner',
                10 => 'Harrier',
                11 => 'HiAce',
                12 => 'Highlander',
                13 => 'Hilux',
                14 => 'Land Cruiser',
                15 => 'Prado',
                16 => 'Prius',
                17 => 'RAV 4',
                18 => 'Sequoia',
                19 => 'Sienna',
                20 => 'Solara',
                21 => 'Surf',
                22 => 'Tundra',
                23 => 'Venza',
                24 => 'Vitz',
                25 => 'Xa',
                26 => 'Yaris',
            ),
        ),

        112 => array(
            'brand' => 'Trabant',
            'model' => array(
                1 => 'P 601',
            ),
        ),

        113 => array(
            'brand' => 'Triumph',
            'model' => array(
                1 => 'Speed Four',
            ),
        ),

        114 => array(
            'brand' => 'UAZ',
            'model' => array(
                1 => '2206',
                2 => '31512',
                3 => '31514',
                4 => '31519',
                5 => '3303',
                6 => '39625',
                7 => '452',
                8 => '469',
                9 => 'Patriot',
                10 => 'Xanter',
            ),
        ),

        115 => array(
            'brand' => 'Ural',
            'model' => array(
                1 => '4320',
                2 => 'DNEPR MT',
            ),
        ),

        116 => array(
            'brand' => 'VAZ',
            'model' => array(
                1 => '2101',
                2 => '21011',
                3 => '21013',
                4 => '2102',
                5 => '2103',
                6 => '2104',
                7 => '2105',
                8 => '2106',
                9 => '2107',
                10 => '2108',
                11 => '2109',
                12 => '21099',
                13 => '2110',
                14 => '2111',
                15 => '2112',
                16 => '2113',
                17 => '2114',
                18 => '2115',
                19 => 'Granta',
                20 => 'Kalina',
                21 => 'Largus',
                22 => 'Niva',
                23 => 'Oka',
                24 => 'Priora',
            ),
        ),

        117 => array(
            'brand' => 'Vespa',
            'model' => array(
                1 => 'Primavera 125',
            ),
        ),

        118 => array(
            'brand' => 'Vespa',
            'model' => array(
                1 => 'Primavera 125',
            ),
        ),

        119 => array(
            'brand' => 'Volkswagen',
            'model' => array(
                1 => 'Amarok',
                2 => 'Beetle',
                3 => 'Bora',
                4 => 'Caddy',
                5 => 'Caravelle',
                6 => 'Crafter',
                7 => 'Golf',
                8 => 'Golf Plus',
                9 => 'Jetta',
                10 => 'Multivan',
                11 => 'Passat',
                12 => 'Passat CC',
                13 => 'Phaeton',
                14 => 'Polo',
                15 => 'Tiguan',
                16 => 'Touareg',
                17 => 'Touran',
                18 => 'Transporter',
                19 => 'Vento',
            ),
        ),

        120 => array(
            'brand' => 'Volvo',
            'model' => array(
                1 => '440',
                2 => '940',
                3 => 'C 70',
                4 => 'FH 13',
                5 => 'FL 619',
                6 => 'S 40',
                7 => 'S 60',
                8 => 'S 70',
                9 => 'S 80',
                10 => 'S 90',
                11 => 'V 70',
                12 => 'Volvo FL 6 H220',
                13 => 'XC 60',
                14 => 'XC 90',
            ),
        ),

        121 => array(
            'brand' => 'Vosxod',
            'model' => array(
                1 => '2',
                2 => '3M',
            ),
        ),

        122 => array(
            'brand' => 'XCMG',
            'model' => array(
                1 => 'QY25K5-I',
            ),
        ),

        123 => array(
            'brand' => 'Yamaha',
            'model' => array(
                1 => 'Crux',
                2 => 'FZ6N',
                3 => 'R15',
                4 => 'Ray Z 115',
                5 => 'XVS1300A Midnight Star',
                6 => 'YBR 125',
                7 => 'YZF-R6',
            ),
        ),

        124 => array(
            'brand' => 'ZAZ',
            'model' => array(
                1 => '965',
                2 => '968M',
                3 => 'Lanos',
                4 => 'Slavuta',
                5 => 'Tavriya',
                6 => 'Vida',
            ),
        ),

        125 => array(
            'brand' => 'ZIL',
            'model' => array(
                1 => '130',
                2 => '131',
                3 => '133',
                4 => '431412',
                5 => '431610',
                6 => '4334',
                7 => '5301',
            ),
        ),

        126 => array(
            'brand' => 'ZX Auto',
            'model' => array(
                1 => 'Landmark',
            ),
        ),

        127 => array(
            'brand' => 'Zontes',
            'model' => array(
                1 => 'Monster 125',
                2 => 'Panther ZT150-8A',
                3 => 'Tiger ZT150-3A',
            ),
        ),
    );

    /**
     * @param int $pageNum
     * @return array
     */
    public function getProducts($pageNum)
    {
        $start = !empty($pageNum)? $pageNum * $this->limit : 0;

        $connection = Yii::$app->db;
        $command = $connection->createCommand("
             SELECT *
             FROM `products`
             ORDER BY `date` DESC
             LIMIT :start, :limit
          "
        );

        $command->bindParam(':start', $start);
        $command->bindParam(':limit', $this->limit);

        $productData =  $command->query();
        $productData = $productData->readAll();

        foreach($productData as $key => $options) {
            $productData[$key]['car_brand_name'] =  $this->carList[$options['car_brand']]['brand'];
            $productData[$key]['car_model_name'] =  $this->carList[$options['car_brand']]['model'][$options['car_model']];
            $productData[$key]['color_name'] =  $this->colors[$options['color']];
            $productData[$key]['city_name'] =  $this->cities[$options['city']];
            $productData[$key]['production_country_name'] = $this->countries[$options['production_country']];
        }

        return $productData;
    }

    /**
     * @return int
     */
    public function getPagesCount()
    {
        $connection = Yii::$app->db;
        $totalCountRows = $connection->createCommand('
            SELECT COUNT(*)
            FROM `products`
        ');

        $totalCountRows = $totalCountRows->queryScalar();
        $numPagination = ceil($totalCountRows / $this->limit);

        return $numPagination;
    }

    /**
     * @param $productId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function deleteProduct($productId)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            DELETE FROM `products`
            WHERE `product_id`=:id
        ');

        $command->bindParam(':id', $productId);
        $command->execute();

        return true;
    }

    /**
     * @param $productId
     * @return array|bool
     */
    public function getProduct($productId)
    {
        $query = new Query;
        $columns = ['product_id', 'product_name', 'manufacturer', 'serial_number', 'car_brand', 'car_model', 'production_year', 'production_country', 'for_year_min', 'for_year_max', 'mileage', 'color', 'price', 'delivery', 'setup', 'editor', 'guarantee', 'credit', 'barter', 'perfect', 'average', 'notwork', 'product_category_type', 'city', 'seller_name', 'seller_mail', 'seller_phone1', 'seller_phone2', 'new', 'utilized', 'moderation','date', 'photo_ids', 'photo_main_id'];

        $query
            ->select($columns)
            ->from('products')
            ->where('product_id=:id', array(':id' => $productId))
            ->limit(1);

        $command = $query->createCommand();
        $productData = $command->queryOne();
        $productData['car_brand_name'] = $this->carList[$productData['car_brand']]['brand'];
        $productData['car_model_name'] = $this->carList[$productData['car_brand']]['model'][$productData['car_model']];
        $productData['color_name'] = $this->colors[$productData['color']];
        $productData['city_name'] = $this->cities[$productData['city']];
        $productData['production_country_name'] = $this->countries[$productData['production_country']];
        $productData['productCategoryTypeName'] = $this->productCategoryType[$productData['product_category_type']];

        if (!empty($productData['photo_ids'])) {
            $productData['photo_ids'] = unserialize($productData['photo_ids']);
        }

        return $productData;
    }


    public function editProduct($productId, $productName, $manufacturer, $serialNumber, $carBrand, $carModel, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city, $sellerName, $sellerMail, $sellerPhone1, $sellerPhone2, $new, $utilized, $moderation, $date, $photoMainId, $photoIds)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            UPDATE `products`
            SET `product_name`       = :productName,
                `manufacturer`       = :manufacturer,
                `serial_number`      = :serialNumber,
                `car_brand`          = :carBrand,
                `car_model`          = :carModel,
                `production_year`    = :productionYear,
                `production_country` = :productionCountry,
                `for_year_min`       = :forYearMin,
                `for_year_max`       = :forYearMax,
                `mileage`            = :mileage,
                `color`              = :color,
                `price`              = :price,
                `delivery`           = :delivery,
                `setup`              = :setup,
                `editor`             = :editor,
                `guarantee`          = :guarantee,
                `credit`             = :credit,
                `barter`             = :barter,
                `perfect`            = :perfect,
                `average`            = :average,
                `notwork`            = :notwork,
                `product_category_type` = :productCategoryType,
                `city`               = :city,
                `seller_name`        = :sellerName,
                `seller_mail`        = :sellerMail,
                `seller_phone1`      = :sellerPhone1,
                `seller_phone2`      = :sellerPhone2,
                `new`                = :new,
                `utilized`           = :utilized,
                `moderation`         = :moderation,
                `date`               = :date,
                `photo_main_id`      = :photoMainId,
                `photo_ids`          = :photoIds
             WHERE `product_id`      = :id
        ');

        $command->bindValues(array(
            ':productName'       => $productName,
            ':manufacturer'      => $manufacturer,
            ':serialNumber'      => $serialNumber,
            ':carBrand'          => $carBrand,
            ':carModel'          => $carModel,
            ':productionYear'    => $productionYear,
            ':productionCountry' => $productionCountry,
            ':forYearMin'        => $forYearMin,
            ':forYearMax'        => $forYearMax,
            ':mileage'           => $mileage,
            ':color'             => $color,
            ':price'             => $price,
            ':delivery'          => $delivery,
            ':setup'             => $setup,
            ':editor'            => $editor,
            ':guarantee'         => $guarantee,
            ':credit'            => $credit,
            ':barter'            => $barter,
            ':perfect'           => $perfect,
            ':average'           => $average,
            ':notwork'           => $notwork,
            ':productCategoryType' => $productCategoryType,
            ':city'              => $city,
            ':sellerName'        => $sellerName,
            ':sellerMail'        => $sellerMail,
            ':sellerPhone1'      => $sellerPhone1,
            ':sellerPhone2'      => $sellerPhone2,
            ':new'               => $new,
            ':utilized'          => $utilized,
            ':moderation'        => $moderation,
            ':date'              => $date,
            ':photoMainId'       => $photoMainId,
            ':photoIds'          => $photoIds,
            ':id'                => $productId,
        ));

        $command->execute();

        return true;
    }

    public function getMessage($sellerMail)
    {
        Yii::$app->mailer->compose('contact/html', ['contactForm' => 'no-reply@oluxana.az'])
            ->setFrom('no-reply@oluxana.az')
            ->setTo($sellerMail)
            ->setSubject('Oluxana bildiriş')
            ->send();

        return true;
    }

    public function addProduct($productName, $manufacturer, $serialNumber, $carBrand, $carModel, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork,  $productCategoryType, $city, $sellerName, $sellerMail, $sellerPhone1, $sellerPhone2, $new, $utilized, $moderation, $date, $photoMainId, $photoIds)
    {
            $connection = Yii::$app->db;
            $command = $connection->createCommand('
            INSERT `products`
            SET `product_name`       = :productName,
                `manufacturer`       = :manufacturer,
                `serial_number`      = :serialNumber,
                `car_brand`          = :carBrand,
                `car_model`          = :carModel,
                `production_year`    = :productionYear,
                `production_country` = :productionCountry,
                `for_year_min`       = :forYearMin,
                `for_year_max`       = :forYearMax,
                `mileage`            = :mileage,
                `color`              = :color,
                `price`              = :price,
                `delivery`           = :delivery,
                `setup`              = :setup,
                `editor`             = :editor,
                `guarantee`          = :guarantee,
                `credit`             = :credit,
                `barter`             = :barter,
                `perfect`            = :perfect,
                `average`            = :average,
                `notwork`            = :notwork,
                `product_category_type` = :productCategoryType,
                `city`               = :city,
                `seller_name`        = :sellerName,
                `seller_mail`        = :sellerMail,
                `seller_phone1`      = :sellerPhone1,
                `seller_phone2`      = :sellerPhone2,
                `new`                = :new,
                `utilized`           = :utilized,
                `moderation`         = :moderation,
                `date`               = :date,
                `photo_main_id`      = :photoMainId,
                `photo_ids`          = :photoIds
        ');

            $command->bindValues(array(
                ':productName'       => $productName,
                ':manufacturer'      => $manufacturer,
                ':serialNumber'      => $serialNumber,
                ':carBrand'          => $carBrand,
                ':carModel'          => $carModel,
                ':productionYear'    => $productionYear,
                ':productionCountry' => $productionCountry,
                ':forYearMin'        => $forYearMin,
                ':forYearMax'        => $forYearMax,
                ':mileage'           => $mileage,
                ':color'             => $color,
                ':price'             => $price,
                ':delivery'          => $delivery,
                ':setup'             => $setup,
                ':editor'            => $editor,
                ':guarantee'         => $guarantee,
                ':credit'            => $credit,
                ':barter'            => $barter,
                ':perfect'           => $perfect,
                ':average'           => $average,
                ':notwork'           => $notwork,
                ':productCategoryType' => $productCategoryType,
                ':city'              => $city,
                ':sellerName'        => $sellerName,
                ':sellerMail'        => $sellerMail,
                ':sellerPhone1'      => $sellerPhone1,
                ':sellerPhone2'      => $sellerPhone2,
                ':new'               => $new,
                ':utilized'          => $utilized,
                ':moderation'        => $moderation,
                ':date'              => $date,
                ':photoMainId'       => $photoMainId,
                ':photoIds'          => $photoIds,
            ));

            $command->execute();

        return true;
    }

    /**
     * @param int $id
     * @return array|bool
     */
    public function getSearchID($id)
    {
        $connection = Yii::$app->db;

        $command = $connection->createCommand('
           SELECT *
           FROM  `products`
           WHERE `product_id` = :id
        ');

        $command->bindValue(':id', $id);
        $productData = $command->queryOne();


        $productData['car_brand_name'] = $this->carList[$productData['car_brand']]['brand'];
        $productData['car_model_name'] = $this->carList[$productData['car_brand']]['model'][$productData['car_model']];
        $productData['color_name'] = $this->colors[$productData['color']];
        $productData['city_name'] = $this->cities[$productData['city']];
        $productData['production_country_name'] = $this->countries[$productData['production_country']];
        $productData['productCategoryTypeName'] = $this->productCategoryType[$productData['product_category_type']];

        return $productData;
    }

    /**
     * @return array
     */
    public function getLinkLimit()
    {
        $pageCountArr = [];

        for ($i = 1; $i <= $this->getPagesCount(); $i++ ) {
            $pageCountArr[$i] = $i;
        }

        $linkLimit = array_chunk($pageCountArr, $this->numberOfPages);

        return $linkLimit;
    }

    /**
     * @return array
     */
    public function getCarList()
    {
        return $this->carList;
    }

    /**
     * @return array
     */
    public function getYearsList()
    {
        return $this->years;
    }

    /**
     * @return array
     */
    public function getCountriesList()
    {
        return $this->countries;
    }

    /**
     * @return array
     */
    public function getproductCategoryType()
    {
        return $this->productCategoryType;
    }

    /**
     * @return array
     */
    public function getColorsList()
    {
        return $this->colors;
    }

    /**
     * @return array
     */
    public function getCitiesList()
    {
        return $this->cities;
    }
}
