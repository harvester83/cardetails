<?php

namespace backend\models;


use Yii;
use yii\base\Model;

class Filechooser extends Model
{
    /**
     * @return array|\yii\db\Command
     */
    public function  getPhotosByMonth()
    {
        $connection = Yii::$app->db;

        $photosByMonth= $connection->createCommand('
            SELECT *
            FROM `filemanager_mediafile`
        ');

        $photosByMonth = $photosByMonth->queryAll();

        foreach ($photosByMonth as $key => $photo) {
            $photosByMonth[$key]['thumbs'] = unserialize($photo['thumbs']);
        }

        return $photosByMonth;
    }
}