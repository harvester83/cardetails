<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class Declaration extends Model
{
    private $limit = 5;

    /**
     * @return array|\yii\db\DataReader
     */
    public function getDeclarations($pageNum)
    {
        $start = !empty($pageNum) ? ($pageNum - 1) * $this->limit : 1;

        $connection = Yii::$app->db;
        $command = $connection->createCommand('
           SELECT `declaration_id`, `moderation`, `product_name`, `editor`, `buyer`, `email`, `phone`, `date`
           FROM `declarations`
           ORDER BY `date` DESC
           LIMIT :start, :limit
        ');

        $command->bindParam(':start', $start);
        $command->bindParam(':limit', $this->limit);

        $declarationData =  $command->query();
        $declarationData = $declarationData->readAll();

        return $declarationData;
    }

    /**
     * @param int $declarationId
     * @return array|bool
     */
    public function getDeclaration($declarationId)
    {
        $connection = Yii::$app->db;

        $command = $connection->createCommand('
           SELECT `declaration_id`, `moderation`, `product_name`, `editor`, `buyer`, `email`, `phone`, `date`
           FROM `declarations`
           WHERE `declaration_id` = :declarationId
        ');

        $command->bindValue(':declarationId', $declarationId);
        $declarationData = $command->queryOne();

        return $declarationData;
    }

    /**
     * @param string $productName
     * @param string $editor
     * @param string $buyer
     * @param string $email
     * @param int $phone
     * @param int $moderation
     * @param int $date
     * @return bool
     * @throws \yii\db\Exception
     */
    public function addDeclaration($productName, $editor, $buyer, $email, $phone, $moderation, $date)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            INSERT `declarations`
            SET `product_name` = :productName,
                `editor` = :editor,
                `buyer` = :buyer,
                `email` = :email,
                `phone` = :phone,
                `moderation` = :moderation,
                `date` = :date
            ');

        $command->bindParam(':productName', $productName);
        $command->bindParam(':editor', $editor);
        $command->bindParam(':buyer', $buyer);
        $command->bindParam(':email', $email);
        $command->bindParam(':phone', $phone);
        $command->bindParam(':moderation', $moderation);
        $command->bindParam(':date', $date);


        $command->execute();

        return true;
    }

    /**
     * @param int $declarationId
     * @param string $productName
     * @param string $editor
     * @param string $buyer
     * @param string $email
     * @param int $phone
     * @param int $moderation
     * @param int $date
     * @return bool
     * @throws \yii\db\Exception
     */
    public function editDeclaration($declarationId, $productName, $editor, $buyer, $email, $phone, $moderation, $date)
    {
        $connection = Yii::$app->db;
        $command = $connection->createCommand('
            UPDATE `declarations`
            SET `product_name` = :productName,
                `editor` = :editor,
                `buyer` = :buyer,
                `email` = :email,
                `phone` = :phone,
                `moderation` = :moderation,
                `date` = :date
            WHERE `declaration_id` = :declarationId
        ');

        $command->bindValue(':productName', $productName);
        $command->bindValue(':editor', $editor);
        $command->bindValue(':buyer', $buyer);
        $command->bindValue(':email', $email);
        $command->bindValue(':phone', $phone);
        $command->bindValue(':moderation', $moderation);
        $command->bindValue(':date', $date);
        $command->bindValue(':declarationId', $declarationId);

        $command->execute();

        return true;
    }

    /**
     * @param int $declarationId
     * @return array|bool
     */
    public function deleteDeclaration($declarationId)
    {
        $connection = Yii::$app->db;

        $command = $connection->createCommand('
           DELETE FROM  `declarations`
           WHERE `declaration_id` = :declarationId
        ');

        $command->bindParam(':declarationId', $declarationId);
        $command->execute();

        return true;
    }

    /**
     * @param int $id
     * @return array|bool
     */
    public function getSearchID($id)
    {
        $connection = Yii::$app->db;

        $command = $connection->createCommand('
           SELECT *
           FROM  `declarations`
           WHERE `declaration_id` = :id
        ');

        $command->bindValue(':id', $id);
        $declarationData = $command->queryOne();

        return $declarationData;
    }

    /**
     * @return int float
     */
    public function getPagesCount()
    {
        $connection = Yii::$app->db;
        $totalCountRows = $connection->createCommand('
            SELECT COUNT(*)
            FROM `declarations`
        ');

        $totalCountRows = $totalCountRows->queryScalar();
        $numPagination = ceil($totalCountRows / $this->limit);

        return $numPagination;
    }
}