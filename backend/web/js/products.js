$(document).ready(function() {

    //change cars model
    $('#car-brand').on('change', function () {
        var carBrandId = $(this).val();
        var carModels = carList[carBrandId]['model'];

        var $carModelWrapper = $('#car-model');
        $carModelWrapper.empty();

        $.each(carModels, function(modelId, carModelName) {
            $($carModelWrapper).append('<option value=" ' + modelId + ' ">' + carModelName + '</option>');
        });
    });

    $('#datetimepicker2').datetimepicker();
    $('.add-product-field').show();

    $('#add-product').click(function() {
        $('.products-list').hide();
        $('#update-product').hide();
        $('.product-download-images ul').hide();
        $('.add-product-field').show();
    });

    $('#save-product').click(function() {
        $('.add-product-field').hide();

        var photoMainId = $('#photos .selected-main-photo').data('photo-id');

        if (!photoMainId) {
            photoMainId = 0;
        }

        var photoIds = {};
        $('#photos .selected-photo').each(function(index) {
            photoIds[index] = $(this).data('photo-id');
        });

        var productCategoryType = $('input:radio[name=category-part]:checked').val();

        var productData = {
            productName:       $('#product-name').val(),
            manufacturer:      $('#manufacturer').val(),
            serialNumber:      $('#serial-number').val(),
            carBrand:          $('#car-brand option:selected').val(),
            carModel:          $('#car-model option:selected').val(),
            productionYear:    $('#production-year option:selected').val(),
            productionCountry: $('#production-country option:selected').val(),
            forYearMin:        $('#for-year-min option:selected').val(),
            forYearMax:        $('#for-year-max option:selected').val(),
            mileage:           $('#mileage').val(),
            color:             $('#color option:selected').val(),
            price:             $('#price').val(),
            delivery:          $('#delivery').prop('checked'),
            setup:             $('#setup').prop('checked'),
            editor:            $('#editor').html(),
            guarantee:         $('#guarantee').prop('checked'),
            credit:            $('#credit').prop('checked'),
            barter:            $('#barter').prop('checked'),
            perfect:           $('#perfect').prop('checked'),
            average:           $('#average').prop('checked'),
            notwork:           $('#notwork').prop('checked'),
            productCategoryType: productCategoryType,
            city:              $('#city option:selected').val(),
            sellerName:        $('#seller-name').val(),
            sellerMail:        $('#seller-mail').val(),
            sellerPhone1:      $('#seller-phone1').val(),
            sellerPhone2:      $('#seller-phone2').val(),
            new:               $('#new').prop('checked'),
            utilized:          $('#utilized').prop('checked'),
            moderation:        $('#moderation').prop('checked'),
            date:              $('#date').val(),
            photoMainId:       photoMainId,
            photoIds:          photoIds
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/add',
            data: JSON.stringify(productData),
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
                $('#update-product').hide();
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Sorry, error', 'error');

                    return false;
                }

                $('.products-list').show();
                $('.add-product').hide();
                $.notify('Add success', 'success');

                // location.reload();
            }
        });

        return false;
    });

    $('button').click(function(){
        $('#car-brand option:selected').val()
    });

    $('#product-table').on('click', 'button.preview-edit-product', function() {
        $('.product-download-images ul').hide();
        $('.products-list').hide();
        $('.add-product-field').show();
        $('#save-product').hide();

        var productData = {
            productId: $(this).closest('tr.product').attr('data-product-id')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/get',
            data: JSON.stringify(productData),
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Sorry, error', 'error');
                    return false;
                }

                var product = data.productData;
                $('#update-product').attr('data-product-id', product.product_id);
                $('#product-name').val(product.product_name);
                $('#manufacturer').val(product.manufacturer);
                $('#serial-number').val(product.serial_number);
                $('#car-brand').append('<option value="'+ product.car_brand +'" selected>' + product.car_brand_name + '</option>');
                $('#car-model').append('<option value="'+ product.car_model +'" selected>' + product.car_model_name + '</option>');
                $('#production-year').val(product.production_year).attr('selected', 'selected');
                $('#production-country').val(product.production_country).attr('selected', 'selected');
                $('#for-year-min').val(product.for_year_min).attr('selected', 'selected');
                $('#for-year-max').val(product.for_year_max).attr('selected', 'selected');
                $('#mileage').val(product.mileage);
                $('#color').val(product.color).attr('selected', 'selected');
                $('#price').val(product.price);
                (product.delivery == 1) ? $('#delivery').attr('checked', 'checked') : false;
                (product.setup == 1) ? $('#setup').attr('checked', 'checked') : false;
                $('#editor').html(product.editor);
                (product.guarantee == 1) ? $('#guarantee').attr('checked', 'checked') : false;
                (product.credit == 1) ? $('#credit').attr('checked', 'checked') : false;
                (product.barter == 1) ? $('#barter').attr('checked', 'checked') : false;
                (product.perfect == 1) ? $('#perfect').attr('checked', 'checked') : false;
                (product.average == 1) ? $('#average').attr('checked', 'checked') : false;
                (product.notwork == 1) ? $('#notwork').attr('checked', 'checked') : false;
                $(''+ product.productCategoryTypeName +'').attr('checked', 'checked');
                $('#city').val(product.city).attr('selected', 'selected');
                $('#seller-name').val(product.seller_name);
                $('#seller-mail').val(product.seller_mail);
                $('#seller-phone1').val(product.seller_phone1);
                $('#seller-phone2').val(product.seller_phone2);
                (product.new == 1) ? $('#new').attr('checked', 'checked') : false;
                (product.utilized == 1) ? $('#utilized').attr('checked', 'checked') : false;
                (product.moderation == 1) ? $('#moderation').attr('checked', 'checked') : false;
                $('#date').val(product.date);

                $('.posts-list').hide();
                $('.add-product').show();

                if (product.photo_ids.length == 0) {
                    return false;
                }

                var productIds = product.photo_ids;
                var photoMainId = product.photo_main_id;
                var photo = $('#photos .photo');

                $.each(photo, function() {
                    var count = productIds.length - 1;
                    $(this).hide();

                    if ($(this).attr('data-photo-id') == photoMainId) {
                        $(this).addClass('selected-main-photo');
                    }

                    for (var i = 0; i <= count; i++ ) {
                        if( $(this).attr('data-photo-id') != productIds[i]) {
                            continue;
                        }

                        $(this).addClass('selected-photo');
                        $(this).show();
                    }
                });
            }
        });

        return false;
    });

    $('.show-images').on('click', function() {
        $('.photo').show();

        return false;
    });



    $('#update-product').click(function() {
        var photoMainId = $('#photos .selected-main-photo').data('photo-id');

        var photoIds = {};
        $('#photos .selected-photo').each(function(index) {
            photoIds[index] = $(this).data('photo-id');
        });

        var productCategoryType = $('.category-button').prop('checked');
        if ($('#interior').prop('checked')) {
            productCategoryType = $('#interior').val();
        }

        if ($('#exterior').prop('checked')) {
            productCategoryType = $('#exterior').val();
        }

        if ($('#general').prop('checked')) {
            productCategoryType = $('#general').val();
        }

        if ($('#action-part').prop('checked')) {
            productCategoryType = $('#action-part').val();
        }

        var postData = {
            productId:         $(this).attr('data-product-id'),
            productName:       $('#product-name').val(),
            manufacturer:      $('#manufacturer').val(),
            serialNumber:      $('#serial-number').val(),
            carBrand:          $('#car-brand option:selected').val(),
            carModel:          $('#car-model option:selected').val(),
            productionYear:    $('#production-year option:selected').val(),
            productionCountry: $('#production-country option:selected').val(),
            forYearMin:        $('#for-year-min option:selected').val(),
            forYearMax:        $('#for-year-max option:selected').val(),
            mileage:           $('#mileage').val(),
            color:             $('#color option:selected').val(),
            price:             $('#price').val(),
            delivery:          $('#delivery').prop('checked'),
            setup:             $('#setup').prop('checked'),
            editor:            $('#editor').html(),
            guarantee:         $('#guarantee').prop('checked'),
            credit:            $('#credit').prop('checked'),
            barter:            $('#barter').prop('checked'),
            perfect:           $('#perfect').prop('checked'),
            average:           $('#average').prop('checked'),
            notwork:           $('#notwork').prop('checked'),
            productCategoryType: productCategoryType,
            city:              $('#city option:selected').val(),
            sellerName:        $('#seller-name').val(),
            sellerMail:        $('#seller-mail').val(),
            sellerPhone1:      $('#seller-phone1').val(),
            sellerPhone2:      $('#seller-phone2').val(),
            new:               $('#new').prop('checked'),
            utilized:          $('#utilized').prop('checked'),
            moderation:        $('#moderation').prop('checked'),
            date:              $('#date').val(),
            photoMainId:       photoMainId,
            photoIds:          photoIds
        };

        $.ajax({
            type: 'POST',
            url: absoluteUrl + '/products/edit',
            data: JSON.stringify(postData),
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
                data = JSON.parse(data);

                if (!data) {
                    $.notify('Sorry, error', 'error');
                    return false;
                }

                $.notify('Update success', 'success');

                //location.reload();
            }
        });

        return false;
    });

    $('#product-table').on('click', 'button.delete-product', function() {
        var el = $(this).closest('.product');
        var data = {
            productId: $(this).closest('tr.product').attr('data-product-id')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/delete',
            data: data,
            success: function(data) {
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Error!', 'error');
                    return false;
                }

                el.remove();

                $.notify('Delete success', 'success');
            }
        });

        return false;
    });

    $('.pagintaion-wrapper').on('click', 'li', function() {
        var paginationIndex = $(this).text() - 1;

        var data = {
            pageNum: paginationIndex
        };

        $.ajax({
            type: 'POST',
            url: absoluteUrl + '/products/list',
            data: data,

            success: function(data) {
                $('#products-item').html(data);
            }
        });

        return false;
    });

    limitLink = JSON.parse(limitLink);
    maxLimitLink = limitLink.length - 1;
    var dataLink = parseInt($('#pagination-posts').attr('data-link'));

    $('.pagintaion-wrapper').on('click', 'span .next', function() {
        if (dataLink != 3) {
            dataLink =  ++dataLink;

            var data = {
                linkLimitIndex:  dataLink
            };

            $.ajax({
                type: 'POST',
                url: absoluteUrl + '/products/listindex',
                data: data,

                success: function(data) {
                    $('.pagintaion-wrapper').html(data);
                }
            });

            return false;
        }

        return false;
    });

    $('.pagintaion-wrapper').on('click', 'span .previous', function() {
        if (dataLink != 0) {
            dataLink =  --dataLink;

            var data = {
                linkLimitIndex:  dataLink
            };

            $.ajax({
                type: 'POST',
                url: absoluteUrl + '/products/listindex',
                data: data,

                success: function(data) {
                    $('.pagintaion-wrapper').html(data);
                }
            });

            return false;
        }

        return false;
    });

    $('.product-search-form-submit').on('click', function() {
        var data = {
            id: $('.product-search-form-text').val()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/products/search',
            data: data,
            success: function(data) {
                $('#products-item').html(data);
            }
        });

        return false;
    });

});

