$(document).ready(function() {
    $('#datetimepicker-photo-list').datetimepicker({
        viewMode: 'months',
        format: 'MM/YYYY'
    }).on('dp.change', function(e) {
        console.log(e);

        var data = {
            timeUploadImages: '11/2015'
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/filechooser/get-images',
            date: data,
            success: function(data) {
                $('#images-by-month').html(data);
            }
        });

        return false
    });

    // Add images for product
    $('.photo img').on('click', function() {
        $(this).parent().addClass('selected-photo');
    });

    $('.photo span.set-main').on('click', function() {
        $(this).parent().addClass('selected-photo');
        $('.photo').removeClass('selected-main-photo');
        $(this).parent().addClass('selected-main-photo');
    });

    $('.photo span.delete').on('click', function() {
        $(this).parent().removeClass('selected-photo');
        $(this).parent().removeClass('selected-main-photo');
    });


    // Edit images for product
    $('.product-images-container').on('click', '.photo', function() {
        $(this).addClass('selected-photo');
    });

    $('.product-images-container').on('click', '.set-main', function() {
        $(this).parent().addClass('selected-photo');
        $(this).parent().addClass('selected-main-photo');
    });

});
