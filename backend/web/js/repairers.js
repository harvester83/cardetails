$(document).ready(function() {
    $('#datetimepicker1').datetimepicker();
    $('.add-post-field').hide();

    $('#add-post').click(function() {
        $('.posts-list').hide();
        $('.add-post-field').show();
        $('#update-post').hide();
    });

    $('#repairers-item').on('click', 'button.preview-edit-post', function() {
        $('.posts-list').hide();
        $('#save-post').hide();
        $('#update-post').show();
        $('.add-post-field').show();

        var data = {
            postId: $(this).closest('tr.post').attr('data-post-id')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/repairers/get',
            data: data,
            success: function(data) {

                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Sorry, error', 'error');
                    return false;
                }

                var post = data.postData;

                $('#update-post').attr('data-post-id', post.repairer_id);

                $('#profile').val(post.profile);
                $('#full_name').val(post.full_name);
                $('#address').val(post.address);
                $('#contact_num').val(post.contact_num);
                $('#date').val(post.date);

                $('.posts-list').hide();
                $('.add-post').show();
            }
        });

        return false;
    });

    $('#update-post').click(function () {
        var data = {
            postId: $(this).attr('data-post-id'),
            profile: $('#profile').val(),
            full_name: $('#full_name').val(),
            address: $('#address').val(),
            contact_num: $('#contact_num').val(),
            date: $('#date').val()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/repairers/edit',
            data: data,
            success: function(data) {
                data = JSON.parse(data);
                console.log(data);

                if (!data.result) {
                    $.notify('Sorry, error', 'error');
                    return false;
                }

                $.notify('Update success', 'success');

                location.reload();
            }

        });

        return false;
    });

    $('#repairers-item').on('click', 'button.delete-post', function() {
        var el = $(this).closest('.post');
        var data = {
           postId: $(this).closest('tr.post').attr('data-post-id')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/repairers/delete',
            data: data,
            success: function(data) {
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Error!', 'error');
                    return false;
                }
                el.remove();
                $.notify('Delete success', 'success');
            }
        });

        return false;
    });

    $('#pagination-repairer').on('click', 'li', function() {
        var data = {
            pageNum: $(this).index()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/repairers/repairers',
            data: data,
            success: function(data) {
                $('#repairers-item').html(data);
            }
        });

        return false;
    });

    // VALIDATION
    $('#add-repairer-form').validate({
        rules: {
            profile: "required",
            full_name: "required",
            address: "required",
            contact_num: "required"
        },
        messages: {
            profile: "* Ixtisasın adını əlavə edin",
            full_name: "* Adınızı qeyt edin",
            address: "* Ünvanızı qeyd edin",
            contact_num: "* Əlaqə nömrənizi qeyd edin"
        },
        submitHandler: function() {
            var data = {
                profile: $('#profile').val(),
                full_name: $('#full_name').val(),
                address: $('#address').val(),
                contact_num: $('#contact_num').val(),
                date: $('#date').val()
            };

            $.ajax({
                type: "POST",
                url: absoluteUrl + '/repairers/add',
                data: data,

                success: function(data) {
                    $('#update-post').hide();
                    data = JSON.parse(data);
                    if (!data.result) {
                        $.notify('Sorry, error', 'error');
                        return false;
                    }
                    $('.posts-list').show();
                    $('.add-post-field').hide();
                    $("#body-table").append(
                        '<tr class="post" data-post-id="' + data.postData[0] + '">' +
                        '<td>' + data.postData[0] + '</td>' +
                        '<td>' + $('#profile').val() + '</td>' +
                        '<td>' + $('#full_name').val() + '</td>' +
                        '<td>' + $('#address').val() + '</td>' +
                        '<td>' + $('#contact_num').val() + '</td>' +
                        '<td>'+ $('#date').val() + '</td>' +
                        '<td>' +
                        '<button type="button" class="preview-edit-post btn btn-warning btn-xs">' +
                        '<span class="glyphicon glyphicon-pencil"></span>' +
                        '</button>' +
                        '</td>' +

                        '<td>' +
                        '<button type="button" class="delete-post btn btn-danger btn-x">' +
                        '<span class="glyphicon glyphicon-remove"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>'
                    );
                    $.notify('Add success', 'success');
                }
            });

            return false;
        }
    });

    $('.repairer-search-form-submit').on('click', function() {
        var data = {
            id: $('.repairer-search-form-text').val()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/repairers/search',
            data: data,
            success: function(data) {
                $('#repairers-item').html(data);
            }
        });

        return false;
    });

});
