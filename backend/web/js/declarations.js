$(document).ready(function() {
    $('.add-declaration-field').hide();
    //$('#declarations-item').on('click', 'button.preview-edit-declaration', function() {
    $('#declaration-table').on('click', '.preview-edit-declaration', function() {
        $('.declarations-list').hide();
        $('.add-declaration-field').show();
        $('#save-declaration').hide();

        var declarationData = {
            declarationId: $(this).closest('tr.declaration').attr('data-declaration-id')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/declarations/get',
            data: JSON.stringify(declarationData),
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Sorry, error', 'error');
                    return false;
                }

                var declaration = data.declaration;

                $('#update-declaration').attr('data-declaration-id', declaration.declaration_id);
                $('#product-name').val(declaration.product_name);
                $('#editor').html(declaration.editor);
                $('#buyer').val(declaration.buyer);
                $('#email').val(declaration.email);
                $('#phone').val(declaration.phone);
                $('#date').val(declaration.date);
                (declaration.moderation == 1) ? $('#moderation').attr('checked', 'checked') : false;
            }
        });

        return false;
    });

    $('#add-declaration').on('click', function() {
        $('.declarations-list').hide();
        $('.add-declaration-field').show();
        $('#update-declaration').hide();
    });


    $('#update-declaration').on('click', function() {
        var declarationData = {
            declarationId: $(this).attr('data-declaration-id'),
            productName: $('#product-name').val(),
            editor: $('#editor').html(),
            buyer: $('#buyer').val(),
            email: $('#email').val(),
            phone: $('#phone').val(),
            moderation: $('#moderation').prop('checked'),
            date: $('#date').val()
        };

        $.ajax({
            type: 'POST',
            url: absoluteUrl + '/declarations/edit',
            data: JSON.stringify(declarationData),
            contentType: 'application/json; charset=UTF-8',
            success: function(data) {
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Elan yenilənmədi. Zəhmət olmasa məlumatları bir daha yoxlayıb göndərəsiz', 'error');
                    return false;
                }

                $.notify('Təşəkkür edirik elanıvız yeniləndi.', 'success');

                //location.reload();
            }
        });

        return false;
    });

    $('#declaration-table').on('click', 'button.delete-declaration', function() {
        var el = $(this).closest('.declaration');
        var declarationData = {
            declarationId: $(this).closest('tr.declaration').attr('data-declaration-id')
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/declarations/delete',
            data: declarationData,
            success: function(data) {
                data = JSON.parse(data);

                if (!data.result) {
                    $.notify('Error!', 'error');
                    return false;
                }

                el.remove();

                $.notify('Delete success', 'success');
            }
        });

        return false;
    });

    //validation
    $('#add-declaration-form').validate({
        rules: {
            productName: "required",
            buyer: "required",
            phone: "required"
        },
        messages: {
            productName: "* Hissənin adını əlavə edin",
            buyer: "* Adınızı qeyt edin",
            phone: "* Əlaqə nömrənizi qeyd edin"
        },

        submitHandler: function() {
            var declarationData = {
                productName: $('#product-name').val(),
                editor: $('#editor').html(),
                buyer: $('#buyer').val(),
                email: $('#email').val(),
                phone: $('#phone').val(),
                moderation: $('#moderation').prop('checked'),
                date: $('#date').val()
            };

            $.ajax({
                type: "POST",
                url: absoluteUrl + '/declarations/add',
                data: declarationData,

                success: function(data) {
                    $('#update-declaration').hide();
                    data = JSON.parse(data);

                    if (!data.result) {
                        $.notify('Elan dərc olunmadı. Zəhmət olmasa məlumatları bir daha yoxlayıb göndərəsiz', 'error');

                        return false;
                    }

                    $('.declarations-list').show();
                    $('.add-declaration').hide();
                    //  $('.add-declaration-field').hide();
                    $.notify('Təşəkkür edirik elanıvız dərc oludu.', 'success');
                }
            });

            return false;
        }
    });

    // propose username by combining first- and lastname
    $("#product-name").focus(function() {
        var productName = $("#product-name").val();
        var buyer = $("#buyer").val();
        if (productName && buyer && !this.value) {
            this.value = productName + "." + buyer;
        }
    });

    $('#pagination-declaration').on('click', 'li', function() {
        var data = {
            pageNum: $(this).index()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/declarations/list',
            data: data,
            success: function(data) {
                $('#declarations-item').html(data);
            }
        });

        return false;
    });

    $('.declaration-search-form-submit').on('click', function() {
        var data = {
            id: $('.declaration-search-form-text').val()
        };

        $.ajax({
            type: "POST",
            url: absoluteUrl + '/declarations/search',
            data: data,
            success: function(data) {
                $('#declaration-table').html(data);
            }
        });

        return false;
    });
});
