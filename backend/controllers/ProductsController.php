<?php
namespace backend\controllers;

use backend\models\Filechooser;
use Yii;
use backend\models\Product;
use Exception;

class ProductsController extends SiteController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $pageNum = $request->post('pageNum', 0);
        $linkLimitIndex = $request->post('linkLimitIndex', 0);
        $products = [];
        $pagesCount = 0;
        $carList = [];
        $years = [];
        $countries = [];
        $colors = [];
        $cities = [];
        $photos = [];
        $linkLimit = [];

        try {
            $linkLimit =  $this->getModel()->getLinkLimit();
            $products = $this->getModel()->getProducts($pageNum);
            $carList = $this->getModel()->getCarList();
            $pagesCount = (int) $this->getModel()->getPagesCount();
            $countries = $this->getModel()->getCountriesList();
            $colors = $this->getModel()->getColorsList();
            $cities = $this->getModel()->getCitiesList();
            $photos = $this->getFileChooserModel()->getPhotosByMonth();

        } catch (Exception $e) {
            Yii::error($e);
        }
        
        return $this->render('index.php', [
            'products' => $products,
            'pagesCount' => $pagesCount,
            'carList' => $carList,
            'years' => $years,
            'countries' => $countries,
            'colors' => $colors,
            'cities' => $cities,
            'photos' => $photos,
            'linkLimitIndex' => $linkLimitIndex,
            'linkLimit' => $linkLimit,
        ]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $request = Yii::$app->request;
        $pageNum = (int) $request->post('pageNum');
        $products = array();

        try {
            $products = $this->getModel()->getProducts($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('products.php', [
            'products' => $products
        ]);
    }

    /**
     * return bool
     */
    public function actionAdd()
    {
        $request = Yii::$app->request;
        $productName = $request->post('productName');
        $manufacturer = $request->post('manufacturer');
        $serialNumber = $request->post('serialNumber');
        $carBrand = (int) $request->post('carBrand');
        $carModel = (int) $request->post('carModel');
        $productionYear = (int) $request->post('productionYear');
        $productionCountry = (int) $request->post('productionCountry');
        $forYearMin = (int) $request->post('forYearMin');
        $forYearMax = (int) $request->post('forYearMax');
        $mileage = (int) $request->post('mileage');
        $color = (int) $request->post('color');
        $price = $request->post('price');
        $delivery = (int) $request->post('delivery');
        $setup = (int) $request->post('setup');
        $editor = $request->post('editor');
        $guarantee = (int) $request->post('guarantee');
        $credit = (int) $request->post('credit');
        $barter = (int) $request->post('barter');
        $perfect = (int) $request->post('perfect');
        $average = (int) $request->post('average');
        $notwork = (int) $request->post('notwork');
        $productCategoryType = (int) $request->post('productCategoryType');
        $city = (int) $request->post('city');
        $sellerName = $request->post('sellerName');
        $sellerMail = $request->post('sellerMail');
        $sellerPhone1 = $request->post('sellerPhone1');
        $sellerPhone2 = $request->post('sellerPhone2');
        $new = (int) $request->post('new');
        $utilized = (int) $request->post('utilized');
        $moderation = (int) $request->post('moderation');
        $date = $request->post('date');
        $photoMainId = $request->post('photoMainId');
        $photoIds = serialize($request->post('photoIds'));
        $productData = array();

        try {
            $productData = (array) $this->getModel()->addProduct($productName, $manufacturer, $serialNumber, $carBrand, $carModel, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city, $sellerName, $sellerMail, $sellerPhone1, $sellerPhone2, $new, $utilized, $moderation, $date, $photoMainId, $photoIds);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $product = array(
            'result'      => $result,
            'productData' => $productData
        );

        echo json_encode($product);
    }

    /**
     * return bool
     */
    public function actionGet()
    {
        $request   = Yii::$app->request;
        $productId = (int) $request->post('productId');

        $productData = [];

        try {
            $productData = $this->getModel()->getProduct($productId);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $product = array(
            'result'      => $result,
            'productData' => $productData
        );

        echo json_encode($product);
    }

    /**
     * return bool
     */
    public function actionEdit()
    {
        $request = Yii::$app->request;

        $productId          = $request->post('productId');
        $productName        = $request->post('productName');
        $manufacturer       = $request->post('manufacturer');
        $serialNumber       = $request->post('serialNumber');
        $carBrand           = (int) $request->post('carBrand');
        $carModel           = (int) $request->post('carModel');
        $productionYear     = (int) $request->post('productionYear');
        $productionCountry  = (int) $request->post('productionCountry');
        $forYearMin         = (int) $request->post('forYearMin');
        $forYearMax         = (int) $request->post('forYearMax');
        $mileage            = (int) $request->post('mileage');
        $color              = (int) $request->post('color');
        $price              = $request->post('price');
        $delivery           = (int) $request->post('delivery');
        $setup              = (int) $request->post('setup');
        $editor             = $request->post('editor');
        $guarantee          = (int) $request->post('guarantee');
        $credit             = (int) $request->post('credit');
        $barter             = (int) $request->post('barter');
        $perfect            = (int) $request->post('perfect');
        $average            = (int) $request->post('average');
        $notwork            = (int) $request->post('notwork');
        $productCategoryType = (int) $request->post('productCategoryType');
        $city               = (int) $request->post('city');
        $sellerName         = $request->post('sellerName');
        $sellerMail         = $request->post('sellerMail');
        $sellerPhone1       = $request->post('sellerPhone1');
        $sellerPhone2       = $request->post('sellerPhone2');
        $new                = (int) $request->post('new');
        $utilized           = (int) $request->post('utilized');
        $moderation         = (int) $request->post('moderation');
        $date               = $request->post('date');
        $photoMainId        = $request->post('photoMainId');
        $photoIds           = serialize($request->post('photoIds'));

        $productData = array();
        try {
            $productData = (array) $this->getModel()->editProduct($productId, $productName, $manufacturer, $serialNumber, $carBrand, $carModel, $productionYear, $productionCountry, $forYearMin, $forYearMax, $mileage, $color, $price, $delivery, $setup, $editor, $guarantee, $credit, $barter, $perfect, $average, $notwork, $productCategoryType, $city, $sellerName, $sellerMail, $sellerPhone1, $sellerPhone2, $new, $utilized, $moderation, $date, $photoMainId, $photoIds);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        if ($moderation) {
            try {
                $this->getModel()->getMessage($sellerMail, $productId);
            } catch (Exception $e) {
                $e->getMessage();
            }
        }

        $product = array(
            'result' => $result,
            'postData' => $productData
        );

        echo json_encode($product);
    }

    /**
     * return bool
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $productId = (int) $request->post('productId', 0);

        $productData = array();

        try {
            $productData = (array) $this->getModel()->deleteProduct($productId);
            $result = true;
        } catch (Exception $e){
            $result = false;
            // todo logining
        }

        $product = array(
            'result' => $result,
            'productData' => $productData
        );

        echo json_encode($product);
    }

    /**
     * @return string
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $id = $request->post('id');

        $pagesCount = 0;
        $data = [];
        $carList = [];
        $years = [];
        $countries = [];
        $colors = [];
        $cities = [];
        $photos = [];

        try {
            $data = $this->getModel()->getSearchID($id);

            $carList = $this->getModel()->getCarList();
            $pagesCount = (int) $this->getModel()->getPagesCount();
            $countries = $this->getModel()->getCountriesList();
            $colors = $this->getModel()->getColorsList();
            $cities = $this->getModel()->getCitiesList();
            $photos = $this->getFileChooserModel()->getPhotosByMonth();
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $product = [
            'data' => $data,
            'result' => $result,
        ];

        return $this->renderPartial('products-search.php', [
            'product' => $product,
            'pagesCount' => $pagesCount,
            'carList' => $carList,
            'years' => $years,
            'countries' => $countries,
            'colors' => $colors,
            'cities' => $cities,
            'photos' => $photos,
            ]
        );
    }

    /**
     *  @return string
     */
    public function actionListindex()
    {
        $request = Yii::$app->request;
        $linkLimit =  $this->getModel()->getLinkLimit();
        $linkLimitIndex = (int) $request->post('linkLimitIndex');

        return $this->renderPartial('pagination', [
            'linkLimitIndex' => $linkLimitIndex,
            'linkLimit' => $linkLimit,
        ]);
    }

    /**
     * @return  Product
     */
    public function getModel()
    {
        return new Product();
    }

    /**
     * @return Filechooser
     */
    public function getFileChooserModel()
    {
        return new Filechooser();
    }
}