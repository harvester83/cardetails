<?php

namespace backend\controllers;

use backend\models\Repairer;
use Exception;
use Yii;

class RepairersController extends SiteController
{
    /**
     * return string
     */
    const REQUEST = 'Yii::$app->request';

    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $pageNum = $request->post('pageNum', 0);
        $posts = array();
        $pagesCount = 0;

        try {
            $posts = $this->getModel()->getRepairers($pageNum);
            $pagesCount = (int)$this->getModel()->getPagesCount();
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('index.twig', ['posts' => $posts, 'pagesCount' => $pagesCount]);
    }

    /**
     * @return string
     */
    public function actionRepairers()
    {
        $request = Yii::$app->request;
        $pageNum = $request->post('pageNum', 0);
        $posts = array();

        try {
            $posts = $this->getModel()->getRepairers($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('repairers.twig', ['posts' => $posts]);
    }

    /**
     * @return bool
     */
    public function actionAdd()
    {
        $request = Yii::$app->request;
        $profile = $request->post('profile');
        $fullName = $request->post('full_name');
        $address = $request->post('address');
        $contactNum = $request->post('contact_num');
        $date = $request->post('date');
        $postData = [];

        try {
            $postData = (array) $this->getModel()->addRepairer($profile, $fullName, $address, $contactNum, $date);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $post = array(
            'result' =>  $result,
            'postData' => $postData
        );

        echo json_encode($post);
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $postId = (int) $request->post('postId', 0);
        $postData = array();

        try {
            $postData = (array) $this->getModel()->deleteRepairer($postId);
            $result = true;
        } catch (Exception $e){
            $result = false;
            // todo logining
        }

        $post = array(
            'result' => $result,
            'postData' => $postData
        );

        echo json_encode($post);
    }

    /**
     * @return bool
     */
    public function actionGet()
    {
        $request = Yii::$app->request;
        $postId = (int) $request->post('postId', 0);
        $postData = [];

        try {
            $postData = $this->getModel()->getRepairer($postId);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $post = array(
            'result' => $result,
            'postData' => $postData
        );

        echo json_encode($post);
    }

    /**
     * return bool
     */
    public function actionEdit()
    {
        $request = Yii::$app->request;

        $postId = (int) $request->post('postId', 0);
        $profile = $request->post('profile');
        $fullName = $request->post('full_name');
        $address = $request->post('address');
        $contactNum = $request->post('contact_num');
        $date = $request->post('date');

        $postData = array();

        try {
            $postData = (array) $this->getModel()->editRepairer($postId, $profile, $fullName, $address, $contactNum, $date);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $post = array(
            'result' => $result,
            'postData' => $postData
        );

        echo json_encode($post);
    }

    /**
     * @return string
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $id = $request->post('id');
        $data = [];

        try {
            $data = $this->getModel()->getSearchID($id);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $data = [
            'repairer' => $data,
            'result' => $result,
        ];

        return $this->renderPartial('repairers-search.twig', ['data' => $data]);
    }

    /**
     * @return Repairer
     */
    private function getModel()
    {
        return new Repairer();
    }
}