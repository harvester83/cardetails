<?php

namespace backend\controllers;

use Yii;
use backend\models\Filechooser;
use Exception;

class FilechooserController extends SiteController
{
    /**
     * @return string
     */
    public function actionGetImages()
    {
        $timeInterval = '01/2015';
        $imagesUrl = array();

        try {
            $imagesUrl = $this->getFileChooserModel()->getPhotosByMonth();
        } catch(Exception $e) {
              Yii:error($e);
        }

        return $this->render('index.php', ['imagesUrl' => $imagesUrl]);
    }

    /**
     * @return Filechooser
     */
    public function getFileChooserModel()
    {
        return new Filechooser();
    }
}