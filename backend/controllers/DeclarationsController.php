<?php

namespace backend\controllers;


use backend\models\Declaration;
use Yii;
use yii\base\Exception;

class DeclarationsController extends SiteController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $pageNum = $request->post('pageNum', 0);
        $pagesCount = 0;
        $declarationData = [];

        try {
            $pagesCount = (int)$this->getModel()->getPagesCount();
            $declarationData = (array)$this->getModel()->getDeclarations($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->render('index', [
            'declarations' => $declarationData,
            'pagesCount' => $pagesCount
        ]);
    }

    /**
     * @return string
     */
    public function actionList()
    {
        $request = Yii::$app->request;
        $pageNum = (int) $request->post('pageNum', 0);

        $declarations = array();

        try {
            $declarations = $this->getModel()->getDeclarations($pageNum);
        } catch (Exception $e) {
            Yii::error($e);
        }

        return $this->renderPartial('declarations.php', ['declarations' => $declarations]);
    }

    /**
     * @return bool
     */
    public function actionAdd()
    {
        $request = Yii::$app->request;
        $productName = $request->post('productName');
        $editor = $request->post('editor');
        $buyer = $request->post('buyer');
        $email = $request->post('email');
        $phone = $request->post('phone');
        $moderation = (int)$request->post('moderation');
        $date = $request->post('date');

        try {
            $declarationData = (array) $this->getModel()->addDeclaration($productName, $editor, $buyer, $email, $phone, $moderation, $date);
            $result = true;
        } catch(Exception $e) {
            $result = false;
        }

        $declaration = array(
            'result'      => $result,
            'declaration' => $declarationData,
        );

        echo json_encode($declaration);
    }

    /**
     * @return bool
     */
    public function actionGet()
    {
        $request = Yii::$app->request;
        $declarationId = $request->post('declarationId');

        try {
            $declarationData = (array)$this->getModel()->getDeclaration($declarationId);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $declaration = [
            'declaration' => $declarationData,
            'result' => $result,
        ];

        echo json_encode($declaration);
    }

    /**
     *  @return bool
     */
    public function actionEdit()
    {
        $request = Yii::$app->request;
        $declarationId = $request->post('declarationId');
        $productName = $request->post('productName');
        $editor = $request->post('editor');
        $buyer = $request->post('buyer');
        $email = $request->post('email');
        $phone = $request->post('phone');
        $moderation = (int)$request->post('moderation');
        $date = $request->post('date');
        $declarationData = [];

        try {
            $declarationData = (array)$this->getModel()->editDeclaration($declarationId, $productName, $editor, $buyer, $email, $phone, $moderation, $date);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $declaration = [
            'declaration' => $declarationData,
            'result' => $result,
        ];

        echo json_encode($declaration);
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $declarationId = $request->post('declarationId');
        $declarationData = [];
        try {
            $declarationData = (array)$this->getModel()->deleteDeclaration($declarationId);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $declaration = [
            'declaration' => $declarationData,
            'result' => $result,
        ];

        echo json_encode($declaration);
    }

    /**
     * @return string
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $id = $request->post('id');

        $declarationData = [];

        try {
            $declarationData = $this->getModel()->getSearchID($id);
            $result = true;
        } catch (Exception $e) {
            $result = false;
        }

        $declarationData = [
            'declaration' => $declarationData,
            'result' => $result,
        ];

        return $this->renderPartial('declarations-search.php', ['declarationData' => $declarationData]);
    }

    /**
     * @return  Declaration
     */
    public function getModel()
    {
        return new Declaration();
    }
}